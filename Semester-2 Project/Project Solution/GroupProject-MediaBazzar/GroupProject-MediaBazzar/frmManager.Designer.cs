﻿namespace GroupProject_MediaBazzar
{
    partial class frmManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmManager));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tcManager = new MetroFramework.Controls.MetroTabControl();
            this.ManageEmployeePage = new MetroFramework.Controls.MetroTabPage();
            this.metroTabControl2 = new MetroFramework.Controls.MetroTabControl();
            this.AddEmployeePage = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.cbSpouse = new MetroFramework.Controls.MetroCheckBox();
            this.cbGender = new System.Windows.Forms.ComboBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.clbLanguages = new System.Windows.Forms.CheckedListBox();
            this.tbStreetName = new MetroFramework.Controls.MetroTextBox();
            this.tbZipcode = new MetroFramework.Controls.MetroTextBox();
            this.lblStreetName = new MetroFramework.Controls.MetroLabel();
            this.lblZipcode = new MetroFramework.Controls.MetroLabel();
            this.btnAddEmployee = new System.Windows.Forms.Button();
            this.tbEmergencyPhoneNumber = new MetroFramework.Controls.MetroTextBox();
            this.tbPhoneNumber = new MetroFramework.Controls.MetroTextBox();
            this.tbNationality = new MetroFramework.Controls.MetroTextBox();
            this.tbEmail = new MetroFramework.Controls.MetroTextBox();
            this.lblEmail = new MetroFramework.Controls.MetroLabel();
            this.lblGender = new MetroFramework.Controls.MetroLabel();
            this.dtpBirthday = new System.Windows.Forms.DateTimePicker();
            this.lblEmergency = new MetroFramework.Controls.MetroLabel();
            this.lblPhoneNumber = new MetroFramework.Controls.MetroLabel();
            this.lblRegion = new MetroFramework.Controls.MetroLabel();
            this.lblNationality = new MetroFramework.Controls.MetroLabel();
            this.tbAddress = new MetroFramework.Controls.MetroTextBox();
            this.tbBSN = new MetroFramework.Controls.MetroTextBox();
            this.tbLastName = new MetroFramework.Controls.MetroTextBox();
            this.tbFirstName = new MetroFramework.Controls.MetroTextBox();
            this.lblBirthday = new MetroFramework.Controls.MetroLabel();
            this.lblBSN = new MetroFramework.Controls.MetroLabel();
            this.lblLastName = new MetroFramework.Controls.MetroLabel();
            this.lblEmployeeName = new MetroFramework.Controls.MetroLabel();
            this.EmployeeContractPage = new MetroFramework.Controls.MetroTabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbFTE = new System.Windows.Forms.ComboBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.dtpStartDatedtp = new System.Windows.Forms.DateTimePicker();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.nudHourlySalary = new System.Windows.Forms.NumericUpDown();
            this.cbbAddContractPosition = new System.Windows.Forms.ComboBox();
            this.cbEndDate = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.lblPosition = new MetroFramework.Controls.MetroLabel();
            this.btnAddContract = new System.Windows.Forms.Button();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.EndDateAddNewContract = new System.Windows.Forms.DateTimePicker();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dtpEndDateRenewContract = new System.Windows.Forms.DateTimePicker();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.btnRenewContract = new System.Windows.Forms.Button();
            this.dgvContracts = new System.Windows.Forms.DataGridView();
            this.contractId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contractPosition = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hourlySalary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.active = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departureReason = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departureType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rbSearchContract = new MetroFramework.Controls.MetroRadioButton();
            this.rbSearchEmployee = new MetroFramework.Controls.MetroRadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cbbDepartureType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpEndContractEndDate = new System.Windows.Forms.DateTimePicker();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.cbDepartureType = new MetroFramework.Controls.MetroLabel();
            this.lblReason = new MetroFramework.Controls.MetroLabel();
            this.tbDepartureReason = new MetroFramework.Controls.MetroTextBox();
            this.btnEndContract = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.nudNewHourlySalary = new System.Windows.Forms.NumericUpDown();
            this.cbbPositionEdit = new System.Windows.Forms.ComboBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblSalary = new MetroFramework.Controls.MetroLabel();
            this.lblnewPosition = new MetroFramework.Controls.MetroLabel();
            this.btnEditContract = new System.Windows.Forms.Button();
            this.gbSearch = new System.Windows.Forms.GroupBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.tbSearchInput = new MetroFramework.Controls.MetroTextBox();
            this.btnSearchForEmployee = new System.Windows.Forms.Button();
            this.dgvEmployees = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bsn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.birthDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nationality = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.spokenLanguages = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.region = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zipCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.streetName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emergencyPhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatisticsPage = new MetroFramework.Controls.MetroTabPage();
            this.gbCompanyStatistics = new System.Windows.Forms.GroupBox();
            this.tbxTotalActiveEmployees = new MetroFramework.Controls.MetroTextBox();
            this.lblTotalEmployees = new MetroFramework.Controls.MetroLabel();
            this.gbStatistics = new System.Windows.Forms.GroupBox();
            this.tbxActiveEmployeesDepartment = new MetroFramework.Controls.MetroTextBox();
            this.tbxRequiredFte = new MetroFramework.Controls.MetroTextBox();
            this.tbxAvailableFte = new MetroFramework.Controls.MetroTextBox();
            this.lblRequiredFte = new MetroFramework.Controls.MetroLabel();
            this.lblAmountOfActiveEmployees = new MetroFramework.Controls.MetroLabel();
            this.lblAvailableFte = new MetroFramework.Controls.MetroLabel();
            this.lblDepartmentStatistics = new MetroFramework.Controls.MetroLabel();
            this.cbbDepartmentStatistics = new System.Windows.Forms.ComboBox();
            this.dgvStatistics = new System.Windows.Forms.DataGridView();
            this.statisticsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statisticsFte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RestockRequestPage = new MetroFramework.Controls.MetroTabPage();
            this.metroTabControl3 = new MetroFramework.Controls.MetroTabControl();
            this.placeRestockRequestPage = new MetroFramework.Controls.MetroTabPage();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.nudProductAmount = new System.Windows.Forms.NumericUpDown();
            this.cbbFloor = new System.Windows.Forms.ComboBox();
            this.lblNumberItems = new MetroFramework.Controls.MetroLabel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.lblFloor = new MetroFramework.Controls.MetroLabel();
            this.btnOrder = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.tbProductSearchInput = new MetroFramework.Controls.MetroTextBox();
            this.btnSearchForItem = new System.Windows.Forms.Button();
            this.dgvProducts = new System.Windows.Forms.DataGridView();
            this.barcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.manufacturerBrand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.category = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.normalPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.promotionPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockInDepot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockOnShelves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sold = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.lbLoggedInAs = new System.Windows.Forms.Label();
            this.cbbModifyContractFte = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tcManager.SuspendLayout();
            this.ManageEmployeePage.SuspendLayout();
            this.metroTabControl2.SuspendLayout();
            this.AddEmployeePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.EmployeeContractPage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHourlySalary)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContracts)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNewHourlySalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.gbSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployees)).BeginInit();
            this.StatisticsPage.SuspendLayout();
            this.gbCompanyStatistics.SuspendLayout();
            this.gbStatistics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStatistics)).BeginInit();
            this.RestockRequestPage.SuspendLayout();
            this.metroTabControl3.SuspendLayout();
            this.placeRestockRequestPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudProductAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProducts)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(152, 98);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // tcManager
            // 
            this.tcManager.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tcManager.Controls.Add(this.ManageEmployeePage);
            this.tcManager.Controls.Add(this.RestockRequestPage);
            this.tcManager.Controls.Add(this.StatisticsPage);
            this.tcManager.CustomBackground = true;
            this.tcManager.ItemSize = new System.Drawing.Size(220, 70);
            this.tcManager.Location = new System.Drawing.Point(4, 100);
            this.tcManager.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tcManager.Name = "tcManager";
            this.tcManager.SelectedIndex = 0;
            this.tcManager.Size = new System.Drawing.Size(1278, 493);
            this.tcManager.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tcManager.Style = MetroFramework.MetroColorStyle.Teal;
            this.tcManager.TabIndex = 3;
            this.tcManager.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tcManager.SelectedIndexChanged += new System.EventHandler(this.TcManager_SelectedIndexChanged);
            // 
            // ManageEmployeePage
            // 
            this.ManageEmployeePage.Controls.Add(this.metroTabControl2);
            this.ManageEmployeePage.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ManageEmployeePage.HorizontalScrollbarBarColor = true;
            this.ManageEmployeePage.HorizontalScrollbarSize = 8;
            this.ManageEmployeePage.Location = new System.Drawing.Point(4, 74);
            this.ManageEmployeePage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ManageEmployeePage.Name = "ManageEmployeePage";
            this.ManageEmployeePage.Size = new System.Drawing.Size(1270, 415);
            this.ManageEmployeePage.TabIndex = 0;
            this.ManageEmployeePage.Text = "Manage Employee";
            this.ManageEmployeePage.VerticalScrollbarBarColor = true;
            this.ManageEmployeePage.VerticalScrollbarSize = 8;
            // 
            // metroTabControl2
            // 
            this.metroTabControl2.Controls.Add(this.AddEmployeePage);
            this.metroTabControl2.Controls.Add(this.EmployeeContractPage);
            this.metroTabControl2.Location = new System.Drawing.Point(2, 2);
            this.metroTabControl2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.metroTabControl2.Name = "metroTabControl2";
            this.metroTabControl2.SelectedIndex = 1;
            this.metroTabControl2.Size = new System.Drawing.Size(1264, 416);
            this.metroTabControl2.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.metroTabControl2.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroTabControl2.TabIndex = 2;
            this.metroTabControl2.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // AddEmployeePage
            // 
            this.AddEmployeePage.Controls.Add(this.metroLabel8);
            this.AddEmployeePage.Controls.Add(this.pictureBox11);
            this.AddEmployeePage.Controls.Add(this.pictureBox7);
            this.AddEmployeePage.Controls.Add(this.cbSpouse);
            this.AddEmployeePage.Controls.Add(this.cbGender);
            this.AddEmployeePage.Controls.Add(this.metroLabel3);
            this.AddEmployeePage.Controls.Add(this.clbLanguages);
            this.AddEmployeePage.Controls.Add(this.tbStreetName);
            this.AddEmployeePage.Controls.Add(this.tbZipcode);
            this.AddEmployeePage.Controls.Add(this.lblStreetName);
            this.AddEmployeePage.Controls.Add(this.lblZipcode);
            this.AddEmployeePage.Controls.Add(this.btnAddEmployee);
            this.AddEmployeePage.Controls.Add(this.tbEmergencyPhoneNumber);
            this.AddEmployeePage.Controls.Add(this.tbPhoneNumber);
            this.AddEmployeePage.Controls.Add(this.tbNationality);
            this.AddEmployeePage.Controls.Add(this.tbEmail);
            this.AddEmployeePage.Controls.Add(this.lblEmail);
            this.AddEmployeePage.Controls.Add(this.lblGender);
            this.AddEmployeePage.Controls.Add(this.dtpBirthday);
            this.AddEmployeePage.Controls.Add(this.lblEmergency);
            this.AddEmployeePage.Controls.Add(this.lblPhoneNumber);
            this.AddEmployeePage.Controls.Add(this.lblRegion);
            this.AddEmployeePage.Controls.Add(this.lblNationality);
            this.AddEmployeePage.Controls.Add(this.tbAddress);
            this.AddEmployeePage.Controls.Add(this.tbBSN);
            this.AddEmployeePage.Controls.Add(this.tbLastName);
            this.AddEmployeePage.Controls.Add(this.tbFirstName);
            this.AddEmployeePage.Controls.Add(this.lblBirthday);
            this.AddEmployeePage.Controls.Add(this.lblBSN);
            this.AddEmployeePage.Controls.Add(this.lblLastName);
            this.AddEmployeePage.Controls.Add(this.lblEmployeeName);
            this.AddEmployeePage.HorizontalScrollbarBarColor = true;
            this.AddEmployeePage.HorizontalScrollbarSize = 8;
            this.AddEmployeePage.Location = new System.Drawing.Point(4, 35);
            this.AddEmployeePage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.AddEmployeePage.Name = "AddEmployeePage";
            this.AddEmployeePage.Size = new System.Drawing.Size(1256, 377);
            this.AddEmployeePage.TabIndex = 0;
            this.AddEmployeePage.Text = "Add Employee";
            this.AddEmployeePage.VerticalScrollbarBarColor = true;
            this.AddEmployeePage.VerticalScrollbarSize = 8;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel8.Location = new System.Drawing.Point(910, 89);
            this.metroLabel8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(123, 20);
            this.metroLabel8.TabIndex = 99;
            this.metroLabel8.Text = "Are you married?";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(47, 165);
            this.pictureBox11.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(111, 92);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 98;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(47, 37);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(111, 93);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 97;
            this.pictureBox7.TabStop = false;
            // 
            // cbSpouse
            // 
            this.cbSpouse.AutoSize = true;
            this.cbSpouse.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.cbSpouse.Location = new System.Drawing.Point(1037, 89);
            this.cbSpouse.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbSpouse.Name = "cbSpouse";
            this.cbSpouse.Size = new System.Drawing.Size(73, 20);
            this.cbSpouse.TabIndex = 96;
            this.cbSpouse.Text = "Spouse";
            this.cbSpouse.UseVisualStyleBackColor = true;
            // 
            // cbGender
            // 
            this.cbGender.FormattingEnabled = true;
            this.cbGender.Items.AddRange(new object[] {
            "Male",
            "Female"});
            this.cbGender.Location = new System.Drawing.Point(337, 118);
            this.cbGender.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbGender.Name = "cbGender";
            this.cbGender.Size = new System.Drawing.Size(151, 21);
            this.cbGender.TabIndex = 68;
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(910, 129);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(125, 20);
            this.metroLabel3.TabIndex = 67;
            this.metroLabel3.Text = "Spoken languages:";
            // 
            // clbLanguages
            // 
            this.clbLanguages.CheckOnClick = true;
            this.clbLanguages.FormattingEnabled = true;
            this.clbLanguages.Items.AddRange(new object[] {
            "Dutch",
            "English",
            "German",
            "French",
            "Arabic"});
            this.clbLanguages.Location = new System.Drawing.Point(910, 161);
            this.clbLanguages.Name = "clbLanguages";
            this.clbLanguages.Size = new System.Drawing.Size(122, 34);
            this.clbLanguages.TabIndex = 66;
            // 
            // tbStreetName
            // 
            this.tbStreetName.Location = new System.Drawing.Point(688, 138);
            this.tbStreetName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbStreetName.Name = "tbStreetName";
            this.tbStreetName.Size = new System.Drawing.Size(150, 20);
            this.tbStreetName.TabIndex = 63;
            // 
            // tbZipcode
            // 
            this.tbZipcode.Location = new System.Drawing.Point(688, 102);
            this.tbZipcode.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbZipcode.Name = "tbZipcode";
            this.tbZipcode.Size = new System.Drawing.Size(150, 20);
            this.tbZipcode.TabIndex = 62;
            // 
            // lblStreetName
            // 
            this.lblStreetName.AutoSize = true;
            this.lblStreetName.Location = new System.Drawing.Point(566, 143);
            this.lblStreetName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStreetName.Name = "lblStreetName";
            this.lblStreetName.Size = new System.Drawing.Size(87, 20);
            this.lblStreetName.TabIndex = 61;
            this.lblStreetName.Text = "Street name:";
            // 
            // lblZipcode
            // 
            this.lblZipcode.AutoSize = true;
            this.lblZipcode.Location = new System.Drawing.Point(566, 106);
            this.lblZipcode.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblZipcode.Name = "lblZipcode";
            this.lblZipcode.Size = new System.Drawing.Size(67, 20);
            this.lblZipcode.TabIndex = 60;
            this.lblZipcode.Text = "Zip code:";
            // 
            // btnAddEmployee
            // 
            this.btnAddEmployee.BackColor = System.Drawing.Color.DarkCyan;
            this.btnAddEmployee.Location = new System.Drawing.Point(568, 280);
            this.btnAddEmployee.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Size = new System.Drawing.Size(366, 48);
            this.btnAddEmployee.TabIndex = 53;
            this.btnAddEmployee.Text = "Add Employee";
            this.btnAddEmployee.UseVisualStyleBackColor = false;
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            // 
            // tbEmergencyPhoneNumber
            // 
            this.tbEmergencyPhoneNumber.Location = new System.Drawing.Point(688, 214);
            this.tbEmergencyPhoneNumber.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbEmergencyPhoneNumber.Name = "tbEmergencyPhoneNumber";
            this.tbEmergencyPhoneNumber.Size = new System.Drawing.Size(150, 20);
            this.tbEmergencyPhoneNumber.TabIndex = 52;
            // 
            // tbPhoneNumber
            // 
            this.tbPhoneNumber.Location = new System.Drawing.Point(688, 176);
            this.tbPhoneNumber.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbPhoneNumber.Name = "tbPhoneNumber";
            this.tbPhoneNumber.Size = new System.Drawing.Size(150, 20);
            this.tbPhoneNumber.TabIndex = 51;
            // 
            // tbNationality
            // 
            this.tbNationality.Location = new System.Drawing.Point(337, 193);
            this.tbNationality.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbNationality.Name = "tbNationality";
            this.tbNationality.Size = new System.Drawing.Size(150, 20);
            this.tbNationality.TabIndex = 50;
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(337, 230);
            this.tbEmail.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(150, 20);
            this.tbEmail.TabIndex = 49;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(252, 230);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(97, 20);
            this.lblEmail.TabIndex = 48;
            this.lblEmail.Text = "Email address:";
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(252, 118);
            this.lblGender.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(60, 20);
            this.lblGender.TabIndex = 47;
            this.lblGender.Text = "Gender:";
            // 
            // dtpBirthday
            // 
            this.dtpBirthday.Location = new System.Drawing.Point(337, 158);
            this.dtpBirthday.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtpBirthday.Name = "dtpBirthday";
            this.dtpBirthday.Size = new System.Drawing.Size(151, 20);
            this.dtpBirthday.TabIndex = 46;
            // 
            // lblEmergency
            // 
            this.lblEmergency.AutoSize = true;
            this.lblEmergency.Location = new System.Drawing.Point(566, 217);
            this.lblEmergency.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEmergency.Name = "lblEmergency";
            this.lblEmergency.Size = new System.Drawing.Size(127, 20);
            this.lblEmergency.TabIndex = 45;
            this.lblEmergency.Text = "Emergency phone:";
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.AutoSize = true;
            this.lblPhoneNumber.Location = new System.Drawing.Point(568, 176);
            this.lblPhoneNumber.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(106, 20);
            this.lblPhoneNumber.TabIndex = 44;
            this.lblPhoneNumber.Text = "Phone number:";
            // 
            // lblRegion
            // 
            this.lblRegion.AutoSize = true;
            this.lblRegion.Location = new System.Drawing.Point(566, 73);
            this.lblRegion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRegion.Name = "lblRegion";
            this.lblRegion.Size = new System.Drawing.Size(55, 20);
            this.lblRegion.TabIndex = 43;
            this.lblRegion.Text = "Region:";
            // 
            // lblNationality
            // 
            this.lblNationality.AutoSize = true;
            this.lblNationality.Location = new System.Drawing.Point(252, 193);
            this.lblNationality.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNationality.Name = "lblNationality";
            this.lblNationality.Size = new System.Drawing.Size(77, 20);
            this.lblNationality.TabIndex = 42;
            this.lblNationality.Text = "Nationality:";
            // 
            // tbAddress
            // 
            this.tbAddress.Location = new System.Drawing.Point(688, 69);
            this.tbAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(150, 20);
            this.tbAddress.TabIndex = 40;
            // 
            // tbBSN
            // 
            this.tbBSN.Location = new System.Drawing.Point(688, 33);
            this.tbBSN.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbBSN.Name = "tbBSN";
            this.tbBSN.Size = new System.Drawing.Size(150, 20);
            this.tbBSN.TabIndex = 39;
            // 
            // tbLastName
            // 
            this.tbLastName.Location = new System.Drawing.Point(337, 85);
            this.tbLastName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(150, 20);
            this.tbLastName.TabIndex = 38;
            // 
            // tbFirstName
            // 
            this.tbFirstName.Location = new System.Drawing.Point(337, 50);
            this.tbFirstName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(150, 20);
            this.tbFirstName.TabIndex = 37;
            // 
            // lblBirthday
            // 
            this.lblBirthday.AutoSize = true;
            this.lblBirthday.Location = new System.Drawing.Point(252, 158);
            this.lblBirthday.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBirthday.Name = "lblBirthday";
            this.lblBirthday.Size = new System.Drawing.Size(72, 20);
            this.lblBirthday.TabIndex = 36;
            this.lblBirthday.Text = "Birth date:";
            // 
            // lblBSN
            // 
            this.lblBSN.AutoSize = true;
            this.lblBSN.Location = new System.Drawing.Point(566, 37);
            this.lblBSN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblBSN.Name = "lblBSN";
            this.lblBSN.Size = new System.Drawing.Size(38, 20);
            this.lblBSN.TabIndex = 35;
            this.lblBSN.Text = "BSN:";
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(252, 85);
            this.lblLastName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(75, 20);
            this.lblLastName.TabIndex = 34;
            this.lblLastName.Text = "Last name:";
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.AutoSize = true;
            this.lblEmployeeName.Location = new System.Drawing.Point(252, 50);
            this.lblEmployeeName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(76, 20);
            this.lblEmployeeName.TabIndex = 33;
            this.lblEmployeeName.Text = "First name:";
            // 
            // EmployeeContractPage
            // 
            this.EmployeeContractPage.Controls.Add(this.groupBox1);
            this.EmployeeContractPage.Controls.Add(this.groupBox6);
            this.EmployeeContractPage.Controls.Add(this.dgvContracts);
            this.EmployeeContractPage.Controls.Add(this.rbSearchContract);
            this.EmployeeContractPage.Controls.Add(this.rbSearchEmployee);
            this.EmployeeContractPage.Controls.Add(this.groupBox4);
            this.EmployeeContractPage.Controls.Add(this.groupBox2);
            this.EmployeeContractPage.Controls.Add(this.gbSearch);
            this.EmployeeContractPage.Controls.Add(this.dgvEmployees);
            this.EmployeeContractPage.HorizontalScrollbarBarColor = true;
            this.EmployeeContractPage.HorizontalScrollbarSize = 8;
            this.EmployeeContractPage.Location = new System.Drawing.Point(4, 35);
            this.EmployeeContractPage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.EmployeeContractPage.Name = "EmployeeContractPage";
            this.EmployeeContractPage.Size = new System.Drawing.Size(1256, 377);
            this.EmployeeContractPage.TabIndex = 2;
            this.EmployeeContractPage.Text = "Contract";
            this.EmployeeContractPage.VerticalScrollbarBarColor = true;
            this.EmployeeContractPage.VerticalScrollbarSize = 8;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.cmbFTE);
            this.groupBox1.Controls.Add(this.metroLabel9);
            this.groupBox1.Controls.Add(this.dtpStartDatedtp);
            this.groupBox1.Controls.Add(this.pictureBox12);
            this.groupBox1.Controls.Add(this.nudHourlySalary);
            this.groupBox1.Controls.Add(this.cbbAddContractPosition);
            this.groupBox1.Controls.Add(this.cbEndDate);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.lblPosition);
            this.groupBox1.Controls.Add(this.btnAddContract);
            this.groupBox1.Controls.Add(this.metroLabel7);
            this.groupBox1.Controls.Add(this.EndDateAddNewContract);
            this.groupBox1.Location = new System.Drawing.Point(995, 158);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(262, 219);
            this.groupBox1.TabIndex = 106;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add Contract";
            // 
            // cmbFTE
            // 
            this.cmbFTE.FormattingEnabled = true;
            this.cmbFTE.Location = new System.Drawing.Point(98, 46);
            this.cmbFTE.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cmbFTE.Name = "cmbFTE";
            this.cmbFTE.Size = new System.Drawing.Size(98, 21);
            this.cmbFTE.TabIndex = 105;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(9, 46);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(35, 20);
            this.metroLabel9.TabIndex = 104;
            this.metroLabel9.Text = "FTE:";
            // 
            // dtpStartDatedtp
            // 
            this.dtpStartDatedtp.Location = new System.Drawing.Point(85, 103);
            this.dtpStartDatedtp.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtpStartDatedtp.Name = "dtpStartDatedtp";
            this.dtpStartDatedtp.Size = new System.Drawing.Size(151, 20);
            this.dtpStartDatedtp.TabIndex = 98;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(14, 170);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(40, 35);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox12.TabIndex = 97;
            this.pictureBox12.TabStop = false;
            // 
            // nudHourlySalary
            // 
            this.nudHourlySalary.DecimalPlaces = 2;
            this.nudHourlySalary.Location = new System.Drawing.Point(98, 17);
            this.nudHourlySalary.Name = "nudHourlySalary";
            this.nudHourlySalary.Size = new System.Drawing.Size(91, 20);
            this.nudHourlySalary.TabIndex = 96;
            // 
            // cbbAddContractPosition
            // 
            this.cbbAddContractPosition.FormattingEnabled = true;
            this.cbbAddContractPosition.Items.AddRange(new object[] {
            "RetailWorker",
            "DepotWorker",
            "Manager",
            "Admin",
            "Security",
            "None"});
            this.cbbAddContractPosition.Location = new System.Drawing.Point(98, 72);
            this.cbbAddContractPosition.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbbAddContractPosition.Name = "cbbAddContractPosition";
            this.cbbAddContractPosition.Size = new System.Drawing.Size(98, 21);
            this.cbbAddContractPosition.TabIndex = 95;
            // 
            // cbEndDate
            // 
            this.cbEndDate.AutoSize = true;
            this.cbEndDate.Location = new System.Drawing.Point(9, 138);
            this.cbEndDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbEndDate.Name = "cbEndDate";
            this.cbEndDate.Size = new System.Drawing.Size(79, 17);
            this.cbEndDate.TabIndex = 94;
            this.cbEndDate.Text = "End date:";
            this.cbEndDate.UseVisualStyleBackColor = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(4, 19);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(94, 20);
            this.metroLabel5.TabIndex = 85;
            this.metroLabel5.Text = "Hourly Salary:";
            // 
            // lblPosition
            // 
            this.lblPosition.AutoSize = true;
            this.lblPosition.Location = new System.Drawing.Point(9, 72);
            this.lblPosition.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPosition.Name = "lblPosition";
            this.lblPosition.Size = new System.Drawing.Size(59, 20);
            this.lblPosition.TabIndex = 82;
            this.lblPosition.Text = "Position:";
            // 
            // btnAddContract
            // 
            this.btnAddContract.BackColor = System.Drawing.Color.DarkCyan;
            this.btnAddContract.Location = new System.Drawing.Point(88, 170);
            this.btnAddContract.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAddContract.Name = "btnAddContract";
            this.btnAddContract.Size = new System.Drawing.Size(147, 35);
            this.btnAddContract.TabIndex = 92;
            this.btnAddContract.Text = "Add Contract";
            this.btnAddContract.UseVisualStyleBackColor = false;
            this.btnAddContract.Click += new System.EventHandler(this.btnAddContract_Click);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(9, 105);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(71, 20);
            this.metroLabel7.TabIndex = 87;
            this.metroLabel7.Text = "Start date:";
            // 
            // EndDateAddNewContract
            // 
            this.EndDateAddNewContract.Location = new System.Drawing.Point(85, 133);
            this.EndDateAddNewContract.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.EndDateAddNewContract.Name = "EndDateAddNewContract";
            this.EndDateAddNewContract.Size = new System.Drawing.Size(151, 20);
            this.EndDateAddNewContract.TabIndex = 89;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.White;
            this.groupBox6.Controls.Add(this.dtpEndDateRenewContract);
            this.groupBox6.Controls.Add(this.metroLabel4);
            this.groupBox6.Controls.Add(this.pictureBox9);
            this.groupBox6.Controls.Add(this.btnRenewContract);
            this.groupBox6.Location = new System.Drawing.Point(951, 11);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox6.Size = new System.Drawing.Size(304, 138);
            this.groupBox6.TabIndex = 105;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Renew Contract";
            // 
            // dtpEndDateRenewContract
            // 
            this.dtpEndDateRenewContract.Location = new System.Drawing.Point(106, 33);
            this.dtpEndDateRenewContract.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtpEndDateRenewContract.Name = "dtpEndDateRenewContract";
            this.dtpEndDateRenewContract.Size = new System.Drawing.Size(151, 20);
            this.dtpEndDateRenewContract.TabIndex = 96;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(4, 36);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(67, 20);
            this.metroLabel4.TabIndex = 95;
            this.metroLabel4.Text = "End date:";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(15, 72);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(40, 45);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 93;
            this.pictureBox9.TabStop = false;
            // 
            // btnRenewContract
            // 
            this.btnRenewContract.BackColor = System.Drawing.Color.DarkCyan;
            this.btnRenewContract.Location = new System.Drawing.Point(106, 72);
            this.btnRenewContract.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRenewContract.Name = "btnRenewContract";
            this.btnRenewContract.Size = new System.Drawing.Size(131, 45);
            this.btnRenewContract.TabIndex = 92;
            this.btnRenewContract.Text = "Renew Contract";
            this.btnRenewContract.UseVisualStyleBackColor = false;
            this.btnRenewContract.Click += new System.EventHandler(this.btnRenewContract_Click);
            // 
            // dgvContracts
            // 
            this.dgvContracts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvContracts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvContracts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.contractId,
            this.contractPosition,
            this.hourlySalary,
            this.startDate,
            this.endDate,
            this.FTE,
            this.active,
            this.departureReason,
            this.departureType});
            this.dgvContracts.Location = new System.Drawing.Point(2, 163);
            this.dgvContracts.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvContracts.Name = "dgvContracts";
            this.dgvContracts.ReadOnly = true;
            this.dgvContracts.RowHeadersWidth = 51;
            this.dgvContracts.RowTemplate.Height = 24;
            this.dgvContracts.Size = new System.Drawing.Size(962, 213);
            this.dgvContracts.TabIndex = 104;
            // 
            // contractId
            // 
            this.contractId.HeaderText = "Id:";
            this.contractId.MinimumWidth = 6;
            this.contractId.Name = "contractId";
            this.contractId.ReadOnly = true;
            // 
            // contractPosition
            // 
            this.contractPosition.HeaderText = "Position:";
            this.contractPosition.MinimumWidth = 6;
            this.contractPosition.Name = "contractPosition";
            this.contractPosition.ReadOnly = true;
            // 
            // hourlySalary
            // 
            this.hourlySalary.HeaderText = "Hourly salary:";
            this.hourlySalary.MinimumWidth = 6;
            this.hourlySalary.Name = "hourlySalary";
            this.hourlySalary.ReadOnly = true;
            // 
            // startDate
            // 
            this.startDate.HeaderText = "Start date:";
            this.startDate.MinimumWidth = 6;
            this.startDate.Name = "startDate";
            this.startDate.ReadOnly = true;
            // 
            // endDate
            // 
            this.endDate.HeaderText = "End date:";
            this.endDate.MinimumWidth = 6;
            this.endDate.Name = "endDate";
            this.endDate.ReadOnly = true;
            // 
            // FTE
            // 
            this.FTE.HeaderText = "FTE";
            this.FTE.MinimumWidth = 6;
            this.FTE.Name = "FTE";
            this.FTE.ReadOnly = true;
            // 
            // active
            // 
            this.active.HeaderText = "Currently active:";
            this.active.MinimumWidth = 6;
            this.active.Name = "active";
            this.active.ReadOnly = true;
            // 
            // departureReason
            // 
            this.departureReason.HeaderText = "Departure reason:";
            this.departureReason.MinimumWidth = 6;
            this.departureReason.Name = "departureReason";
            this.departureReason.ReadOnly = true;
            // 
            // departureType
            // 
            this.departureType.HeaderText = "Departure type:";
            this.departureType.MinimumWidth = 6;
            this.departureType.Name = "departureType";
            this.departureType.ReadOnly = true;
            // 
            // rbSearchContract
            // 
            this.rbSearchContract.AutoSize = true;
            this.rbSearchContract.Location = new System.Drawing.Point(144, 147);
            this.rbSearchContract.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rbSearchContract.Name = "rbSearchContract";
            this.rbSearchContract.Size = new System.Drawing.Size(73, 17);
            this.rbSearchContract.TabIndex = 103;
            this.rbSearchContract.TabStop = true;
            this.rbSearchContract.Text = "Contract";
            this.rbSearchContract.UseVisualStyleBackColor = true;
            this.rbSearchContract.CheckedChanged += new System.EventHandler(this.rbSearchContract_CheckedChanged);
            // 
            // rbSearchEmployee
            // 
            this.rbSearchEmployee.AutoSize = true;
            this.rbSearchEmployee.Location = new System.Drawing.Point(50, 147);
            this.rbSearchEmployee.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.rbSearchEmployee.Name = "rbSearchEmployee";
            this.rbSearchEmployee.Size = new System.Drawing.Size(81, 17);
            this.rbSearchEmployee.TabIndex = 102;
            this.rbSearchEmployee.TabStop = true;
            this.rbSearchEmployee.Text = "Employee";
            this.rbSearchEmployee.UseVisualStyleBackColor = true;
            this.rbSearchEmployee.CheckedChanged += new System.EventHandler(this.rbSearchEmployee_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.White;
            this.groupBox4.Controls.Add(this.cbbDepartureType);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.dtpEndContractEndDate);
            this.groupBox4.Controls.Add(this.pictureBox6);
            this.groupBox4.Controls.Add(this.cbDepartureType);
            this.groupBox4.Controls.Add(this.lblReason);
            this.groupBox4.Controls.Add(this.tbDepartureReason);
            this.groupBox4.Controls.Add(this.btnEndContract);
            this.groupBox4.Location = new System.Drawing.Point(255, 11);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Size = new System.Drawing.Size(328, 138);
            this.groupBox4.TabIndex = 101;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "End contract";
            // 
            // cbbDepartureType
            // 
            this.cbbDepartureType.FormattingEnabled = true;
            this.cbbDepartureType.Items.AddRange(new object[] {
            "Fired",
            "Quit"});
            this.cbbDepartureType.Location = new System.Drawing.Point(230, 52);
            this.cbbDepartureType.Name = "cbbDepartureType";
            this.cbbDepartureType.Size = new System.Drawing.Size(93, 21);
            this.cbbDepartureType.TabIndex = 70;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 15);
            this.label1.TabIndex = 69;
            this.label1.Text = "End date";
            // 
            // dtpEndContractEndDate
            // 
            this.dtpEndContractEndDate.Location = new System.Drawing.Point(123, 24);
            this.dtpEndContractEndDate.Name = "dtpEndContractEndDate";
            this.dtpEndContractEndDate.Size = new System.Drawing.Size(200, 20);
            this.dtpEndContractEndDate.TabIndex = 68;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(12, 19);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(32, 30);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 67;
            this.pictureBox6.TabStop = false;
            // 
            // cbDepartureType
            // 
            this.cbDepartureType.AutoSize = true;
            this.cbDepartureType.Location = new System.Drawing.Point(123, 54);
            this.cbDepartureType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.cbDepartureType.Name = "cbDepartureType";
            this.cbDepartureType.Size = new System.Drawing.Size(107, 20);
            this.cbDepartureType.TabIndex = 63;
            this.cbDepartureType.Text = "Departure type:";
            // 
            // lblReason
            // 
            this.lblReason.AutoSize = true;
            this.lblReason.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblReason.Location = new System.Drawing.Point(12, 54);
            this.lblReason.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(116, 20);
            this.lblReason.TabIndex = 65;
            this.lblReason.Text = "Add a comment";
            // 
            // tbDepartureReason
            // 
            this.tbDepartureReason.Location = new System.Drawing.Point(12, 90);
            this.tbDepartureReason.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbDepartureReason.Name = "tbDepartureReason";
            this.tbDepartureReason.Size = new System.Drawing.Size(208, 41);
            this.tbDepartureReason.TabIndex = 66;
            // 
            // btnEndContract
            // 
            this.btnEndContract.BackColor = System.Drawing.Color.DarkCyan;
            this.btnEndContract.Location = new System.Drawing.Point(241, 86);
            this.btnEndContract.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEndContract.Name = "btnEndContract";
            this.btnEndContract.Size = new System.Drawing.Size(83, 41);
            this.btnEndContract.TabIndex = 64;
            this.btnEndContract.Text = "End contract";
            this.btnEndContract.UseVisualStyleBackColor = false;
            this.btnEndContract.Click += new System.EventHandler(this.btnEndContract_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.cbbModifyContractFte);
            this.groupBox2.Controls.Add(this.metroLabel10);
            this.groupBox2.Controls.Add(this.nudNewHourlySalary);
            this.groupBox2.Controls.Add(this.cbbPositionEdit);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.pictureBox2);
            this.groupBox2.Controls.Add(this.lblSalary);
            this.groupBox2.Controls.Add(this.lblnewPosition);
            this.groupBox2.Controls.Add(this.btnEditContract);
            this.groupBox2.Location = new System.Drawing.Point(601, 11);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(329, 138);
            this.groupBox2.TabIndex = 100;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Modify Employee Contract";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(76, 90);
            this.metroLabel10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(35, 20);
            this.metroLabel10.TabIndex = 107;
            this.metroLabel10.Text = "FTE:";
            // 
            // nudNewHourlySalary
            // 
            this.nudNewHourlySalary.DecimalPlaces = 2;
            this.nudNewHourlySalary.Location = new System.Drawing.Point(136, 25);
            this.nudNewHourlySalary.Name = "nudNewHourlySalary";
            this.nudNewHourlySalary.Size = new System.Drawing.Size(178, 20);
            this.nudNewHourlySalary.TabIndex = 97;
            // 
            // cbbPositionEdit
            // 
            this.cbbPositionEdit.FormattingEnabled = true;
            this.cbbPositionEdit.Items.AddRange(new object[] {
            "RetailWorker",
            "DepotWorker",
            "Manager",
            "Admin",
            "Security",
            "None"});
            this.cbbPositionEdit.Location = new System.Drawing.Point(137, 53);
            this.cbbPositionEdit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbbPositionEdit.Name = "cbbPositionEdit";
            this.cbbPositionEdit.Size = new System.Drawing.Size(177, 21);
            this.cbbPositionEdit.TabIndex = 97;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(11, 24);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(48, 48);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 96;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(11, 83);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(48, 48);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 95;
            this.pictureBox2.TabStop = false;
            // 
            // lblSalary
            // 
            this.lblSalary.AutoSize = true;
            this.lblSalary.Location = new System.Drawing.Point(76, 24);
            this.lblSalary.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSalary.Name = "lblSalary";
            this.lblSalary.Size = new System.Drawing.Size(49, 20);
            this.lblSalary.TabIndex = 90;
            this.lblSalary.Text = "Salary:";
            // 
            // lblnewPosition
            // 
            this.lblnewPosition.AutoSize = true;
            this.lblnewPosition.Location = new System.Drawing.Point(76, 53);
            this.lblnewPosition.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblnewPosition.Name = "lblnewPosition";
            this.lblnewPosition.Size = new System.Drawing.Size(59, 20);
            this.lblnewPosition.TabIndex = 91;
            this.lblnewPosition.Text = "Position:";
            // 
            // btnEditContract
            // 
            this.btnEditContract.BackColor = System.Drawing.Color.DarkCyan;
            this.btnEditContract.Location = new System.Drawing.Point(218, 82);
            this.btnEditContract.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEditContract.Name = "btnEditContract";
            this.btnEditContract.Size = new System.Drawing.Size(96, 49);
            this.btnEditContract.TabIndex = 94;
            this.btnEditContract.Text = "Edit contract and save";
            this.btnEditContract.UseVisualStyleBackColor = false;
            this.btnEditContract.Click += new System.EventHandler(this.btnEditContract_Click);
            // 
            // gbSearch
            // 
            this.gbSearch.BackColor = System.Drawing.Color.White;
            this.gbSearch.Controls.Add(this.pictureBox5);
            this.gbSearch.Controls.Add(this.metroLabel6);
            this.gbSearch.Controls.Add(this.tbSearchInput);
            this.gbSearch.Controls.Add(this.btnSearchForEmployee);
            this.gbSearch.Location = new System.Drawing.Point(2, 11);
            this.gbSearch.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbSearch.Name = "gbSearch";
            this.gbSearch.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbSearch.Size = new System.Drawing.Size(236, 131);
            this.gbSearch.TabIndex = 93;
            this.gbSearch.TabStop = false;
            this.gbSearch.Text = "Search";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(9, 23);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(38, 40);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 83;
            this.pictureBox5.TabStop = false;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel6.Location = new System.Drawing.Point(64, 33);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(46, 25);
            this.metroLabel6.TabIndex = 81;
            this.metroLabel6.Text = "Info:";
            // 
            // tbSearchInput
            // 
            this.tbSearchInput.Location = new System.Drawing.Point(113, 33);
            this.tbSearchInput.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbSearchInput.Name = "tbSearchInput";
            this.tbSearchInput.Size = new System.Drawing.Size(107, 19);
            this.tbSearchInput.TabIndex = 75;
            // 
            // btnSearchForEmployee
            // 
            this.btnSearchForEmployee.BackColor = System.Drawing.Color.DarkCyan;
            this.btnSearchForEmployee.Location = new System.Drawing.Point(64, 72);
            this.btnSearchForEmployee.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSearchForEmployee.Name = "btnSearchForEmployee";
            this.btnSearchForEmployee.Size = new System.Drawing.Size(157, 35);
            this.btnSearchForEmployee.TabIndex = 76;
            this.btnSearchForEmployee.Text = "Search Employee";
            this.btnSearchForEmployee.UseVisualStyleBackColor = false;
            this.btnSearchForEmployee.Click += new System.EventHandler(this.btnSearchForEmployee_Click);
            // 
            // dgvEmployees
            // 
            this.dgvEmployees.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEmployees.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployees.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.firstName,
            this.lastName,
            this.gender,
            this.bsn,
            this.birthDate,
            this.emailAddress,
            this.nationality,
            this.spokenLanguages,
            this.region,
            this.zipCode,
            this.streetName,
            this.phoneNumber,
            this.emergencyPhoneNumber});
            this.dgvEmployees.Location = new System.Drawing.Point(2, 163);
            this.dgvEmployees.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvEmployees.Name = "dgvEmployees";
            this.dgvEmployees.RowHeadersWidth = 51;
            this.dgvEmployees.RowTemplate.Height = 24;
            this.dgvEmployees.Size = new System.Drawing.Size(962, 210);
            this.dgvEmployees.TabIndex = 91;
            // 
            // id
            // 
            this.id.HeaderText = "Id:";
            this.id.MinimumWidth = 6;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // firstName
            // 
            this.firstName.HeaderText = "First name:";
            this.firstName.MinimumWidth = 6;
            this.firstName.Name = "firstName";
            this.firstName.ReadOnly = true;
            // 
            // lastName
            // 
            this.lastName.HeaderText = "Last name:";
            this.lastName.MinimumWidth = 6;
            this.lastName.Name = "lastName";
            this.lastName.ReadOnly = true;
            // 
            // gender
            // 
            this.gender.HeaderText = "Gender:";
            this.gender.MinimumWidth = 6;
            this.gender.Name = "gender";
            this.gender.ReadOnly = true;
            // 
            // bsn
            // 
            this.bsn.HeaderText = "BSN:";
            this.bsn.MinimumWidth = 6;
            this.bsn.Name = "bsn";
            this.bsn.ReadOnly = true;
            // 
            // birthDate
            // 
            this.birthDate.HeaderText = "Birthdate:";
            this.birthDate.MinimumWidth = 6;
            this.birthDate.Name = "birthDate";
            this.birthDate.ReadOnly = true;
            // 
            // emailAddress
            // 
            this.emailAddress.HeaderText = "Email:";
            this.emailAddress.MinimumWidth = 6;
            this.emailAddress.Name = "emailAddress";
            this.emailAddress.ReadOnly = true;
            // 
            // nationality
            // 
            this.nationality.HeaderText = "Nationality:";
            this.nationality.MinimumWidth = 6;
            this.nationality.Name = "nationality";
            this.nationality.ReadOnly = true;
            // 
            // spokenLanguages
            // 
            this.spokenLanguages.HeaderText = "Spoken languages:";
            this.spokenLanguages.MinimumWidth = 6;
            this.spokenLanguages.Name = "spokenLanguages";
            this.spokenLanguages.ReadOnly = true;
            // 
            // region
            // 
            this.region.HeaderText = "Region:";
            this.region.MinimumWidth = 6;
            this.region.Name = "region";
            this.region.ReadOnly = true;
            // 
            // zipCode
            // 
            this.zipCode.HeaderText = "Zipcode:";
            this.zipCode.MinimumWidth = 6;
            this.zipCode.Name = "zipCode";
            this.zipCode.ReadOnly = true;
            // 
            // streetName
            // 
            this.streetName.HeaderText = "Street name:";
            this.streetName.MinimumWidth = 6;
            this.streetName.Name = "streetName";
            this.streetName.ReadOnly = true;
            // 
            // phoneNumber
            // 
            this.phoneNumber.HeaderText = "Phone number:";
            this.phoneNumber.MinimumWidth = 6;
            this.phoneNumber.Name = "phoneNumber";
            this.phoneNumber.ReadOnly = true;
            // 
            // emergencyPhoneNumber
            // 
            this.emergencyPhoneNumber.HeaderText = "Emergency phone number:";
            this.emergencyPhoneNumber.MinimumWidth = 6;
            this.emergencyPhoneNumber.Name = "emergencyPhoneNumber";
            this.emergencyPhoneNumber.ReadOnly = true;
            // 
            // StatisticsPage
            // 
            this.StatisticsPage.Controls.Add(this.gbCompanyStatistics);
            this.StatisticsPage.Controls.Add(this.gbStatistics);
            this.StatisticsPage.Controls.Add(this.lblDepartmentStatistics);
            this.StatisticsPage.Controls.Add(this.cbbDepartmentStatistics);
            this.StatisticsPage.Controls.Add(this.dgvStatistics);
            this.StatisticsPage.HorizontalScrollbarBarColor = true;
            this.StatisticsPage.HorizontalScrollbarSize = 8;
            this.StatisticsPage.Location = new System.Drawing.Point(4, 74);
            this.StatisticsPage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.StatisticsPage.Name = "StatisticsPage";
            this.StatisticsPage.Size = new System.Drawing.Size(1270, 415);
            this.StatisticsPage.TabIndex = 2;
            this.StatisticsPage.Text = "Statistics";
            this.StatisticsPage.VerticalScrollbarBarColor = true;
            this.StatisticsPage.VerticalScrollbarSize = 8;
            // 
            // gbCompanyStatistics
            // 
            this.gbCompanyStatistics.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gbCompanyStatistics.Controls.Add(this.tbxTotalActiveEmployees);
            this.gbCompanyStatistics.Controls.Add(this.lblTotalEmployees);
            this.gbCompanyStatistics.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCompanyStatistics.Location = new System.Drawing.Point(586, 30);
            this.gbCompanyStatistics.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbCompanyStatistics.Name = "gbCompanyStatistics";
            this.gbCompanyStatistics.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbCompanyStatistics.Size = new System.Drawing.Size(520, 144);
            this.gbCompanyStatistics.TabIndex = 6;
            this.gbCompanyStatistics.TabStop = false;
            this.gbCompanyStatistics.Text = "Company statistics";
            // 
            // tbxTotalActiveEmployees
            // 
            this.tbxTotalActiveEmployees.Location = new System.Drawing.Point(274, 41);
            this.tbxTotalActiveEmployees.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbxTotalActiveEmployees.Name = "tbxTotalActiveEmployees";
            this.tbxTotalActiveEmployees.ReadOnly = true;
            this.tbxTotalActiveEmployees.Size = new System.Drawing.Size(162, 19);
            this.tbxTotalActiveEmployees.TabIndex = 1;
            // 
            // lblTotalEmployees
            // 
            this.lblTotalEmployees.AutoSize = true;
            this.lblTotalEmployees.Location = new System.Drawing.Point(14, 41);
            this.lblTotalEmployees.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotalEmployees.Name = "lblTotalEmployees";
            this.lblTotalEmployees.Size = new System.Drawing.Size(219, 20);
            this.lblTotalEmployees.TabIndex = 0;
            this.lblTotalEmployees.Text = "Total amount of active employees:";
            // 
            // gbStatistics
            // 
            this.gbStatistics.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gbStatistics.Controls.Add(this.tbxActiveEmployeesDepartment);
            this.gbStatistics.Controls.Add(this.tbxRequiredFte);
            this.gbStatistics.Controls.Add(this.tbxAvailableFte);
            this.gbStatistics.Controls.Add(this.lblRequiredFte);
            this.gbStatistics.Controls.Add(this.lblAmountOfActiveEmployees);
            this.gbStatistics.Controls.Add(this.lblAvailableFte);
            this.gbStatistics.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbStatistics.Location = new System.Drawing.Point(586, 188);
            this.gbStatistics.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbStatistics.Name = "gbStatistics";
            this.gbStatistics.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbStatistics.Size = new System.Drawing.Size(520, 154);
            this.gbStatistics.TabIndex = 5;
            this.gbStatistics.TabStop = false;
            this.gbStatistics.Text = "Statistics of chosen department";
            // 
            // tbxActiveEmployeesDepartment
            // 
            this.tbxActiveEmployeesDepartment.Location = new System.Drawing.Point(274, 98);
            this.tbxActiveEmployeesDepartment.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbxActiveEmployeesDepartment.Name = "tbxActiveEmployeesDepartment";
            this.tbxActiveEmployeesDepartment.ReadOnly = true;
            this.tbxActiveEmployeesDepartment.Size = new System.Drawing.Size(162, 19);
            this.tbxActiveEmployeesDepartment.TabIndex = 4;
            // 
            // tbxRequiredFte
            // 
            this.tbxRequiredFte.Location = new System.Drawing.Point(274, 74);
            this.tbxRequiredFte.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbxRequiredFte.Name = "tbxRequiredFte";
            this.tbxRequiredFte.ReadOnly = true;
            this.tbxRequiredFte.Size = new System.Drawing.Size(162, 19);
            this.tbxRequiredFte.TabIndex = 3;
            // 
            // tbxAvailableFte
            // 
            this.tbxAvailableFte.Location = new System.Drawing.Point(274, 50);
            this.tbxAvailableFte.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbxAvailableFte.Name = "tbxAvailableFte";
            this.tbxAvailableFte.ReadOnly = true;
            this.tbxAvailableFte.Size = new System.Drawing.Size(162, 19);
            this.tbxAvailableFte.TabIndex = 2;
            // 
            // lblRequiredFte
            // 
            this.lblRequiredFte.AutoSize = true;
            this.lblRequiredFte.Location = new System.Drawing.Point(24, 74);
            this.lblRequiredFte.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRequiredFte.Name = "lblRequiredFte";
            this.lblRequiredFte.Size = new System.Drawing.Size(223, 20);
            this.lblRequiredFte.TabIndex = 2;
            this.lblRequiredFte.Text = "Expected required shifts per week:";
            // 
            // lblAmountOfActiveEmployees
            // 
            this.lblAmountOfActiveEmployees.AutoSize = true;
            this.lblAmountOfActiveEmployees.Location = new System.Drawing.Point(24, 98);
            this.lblAmountOfActiveEmployees.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAmountOfActiveEmployees.Name = "lblAmountOfActiveEmployees";
            this.lblAmountOfActiveEmployees.Size = new System.Drawing.Size(193, 20);
            this.lblAmountOfActiveEmployees.TabIndex = 1;
            this.lblAmountOfActiveEmployees.Text = "Amount of active employees: ";
            // 
            // lblAvailableFte
            // 
            this.lblAvailableFte.AutoSize = true;
            this.lblAvailableFte.Location = new System.Drawing.Point(24, 50);
            this.lblAvailableFte.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAvailableFte.Name = "lblAvailableFte";
            this.lblAvailableFte.Size = new System.Drawing.Size(165, 20);
            this.lblAvailableFte.TabIndex = 0;
            this.lblAvailableFte.Text = "Available employee shifts";
            // 
            // lblDepartmentStatistics
            // 
            this.lblDepartmentStatistics.AutoSize = true;
            this.lblDepartmentStatistics.Location = new System.Drawing.Point(20, 46);
            this.lblDepartmentStatistics.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDepartmentStatistics.Name = "lblDepartmentStatistics";
            this.lblDepartmentStatistics.Size = new System.Drawing.Size(87, 20);
            this.lblDepartmentStatistics.TabIndex = 4;
            this.lblDepartmentStatistics.Text = "Department:";
            // 
            // cbbDepartmentStatistics
            // 
            this.cbbDepartmentStatistics.FormattingEnabled = true;
            this.cbbDepartmentStatistics.Location = new System.Drawing.Point(152, 46);
            this.cbbDepartmentStatistics.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbbDepartmentStatistics.Name = "cbbDepartmentStatistics";
            this.cbbDepartmentStatistics.Size = new System.Drawing.Size(158, 21);
            this.cbbDepartmentStatistics.TabIndex = 3;
            this.cbbDepartmentStatistics.SelectedIndexChanged += new System.EventHandler(this.CbbDepartmentStatistics_SelectedIndexChanged);
            // 
            // dgvStatistics
            // 
            this.dgvStatistics.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStatistics.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.statisticsName,
            this.statisticsFte});
            this.dgvStatistics.Location = new System.Drawing.Point(2, 103);
            this.dgvStatistics.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvStatistics.Name = "dgvStatistics";
            this.dgvStatistics.RowHeadersWidth = 51;
            this.dgvStatistics.RowTemplate.Height = 24;
            this.dgvStatistics.Size = new System.Drawing.Size(308, 240);
            this.dgvStatistics.TabIndex = 2;
            // 
            // statisticsName
            // 
            this.statisticsName.HeaderText = "Name:";
            this.statisticsName.MinimumWidth = 6;
            this.statisticsName.Name = "statisticsName";
            this.statisticsName.ReadOnly = true;
            this.statisticsName.Width = 125;
            // 
            // statisticsFte
            // 
            this.statisticsFte.HeaderText = "FTE:";
            this.statisticsFte.MinimumWidth = 6;
            this.statisticsFte.Name = "statisticsFte";
            this.statisticsFte.ReadOnly = true;
            this.statisticsFte.Width = 125;
            // 
            // RestockRequestPage
            // 
            this.RestockRequestPage.Controls.Add(this.metroTabControl3);
            this.RestockRequestPage.HorizontalScrollbarBarColor = true;
            this.RestockRequestPage.HorizontalScrollbarSize = 8;
            this.RestockRequestPage.Location = new System.Drawing.Point(4, 74);
            this.RestockRequestPage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.RestockRequestPage.Name = "RestockRequestPage";
            this.RestockRequestPage.Size = new System.Drawing.Size(1270, 415);
            this.RestockRequestPage.TabIndex = 1;
            this.RestockRequestPage.Text = "Restock Request";
            this.RestockRequestPage.VerticalScrollbarBarColor = true;
            this.RestockRequestPage.VerticalScrollbarSize = 8;
            // 
            // metroTabControl3
            // 
            this.metroTabControl3.Controls.Add(this.placeRestockRequestPage);
            this.metroTabControl3.Location = new System.Drawing.Point(2, 0);
            this.metroTabControl3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.metroTabControl3.Name = "metroTabControl3";
            this.metroTabControl3.SelectedIndex = 0;
            this.metroTabControl3.Size = new System.Drawing.Size(1258, 401);
            this.metroTabControl3.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroTabControl3.TabIndex = 2;
            // 
            // placeRestockRequestPage
            // 
            this.placeRestockRequestPage.Controls.Add(this.pictureBox10);
            this.placeRestockRequestPage.Controls.Add(this.groupBox5);
            this.placeRestockRequestPage.Controls.Add(this.groupBox3);
            this.placeRestockRequestPage.Controls.Add(this.dgvProducts);
            this.placeRestockRequestPage.HorizontalScrollbarBarColor = true;
            this.placeRestockRequestPage.HorizontalScrollbarSize = 8;
            this.placeRestockRequestPage.Location = new System.Drawing.Point(4, 35);
            this.placeRestockRequestPage.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.placeRestockRequestPage.Name = "placeRestockRequestPage";
            this.placeRestockRequestPage.Size = new System.Drawing.Size(1250, 362);
            this.placeRestockRequestPage.TabIndex = 0;
            this.placeRestockRequestPage.Text = "Place restock request";
            this.placeRestockRequestPage.VerticalScrollbarBarColor = true;
            this.placeRestockRequestPage.VerticalScrollbarSize = 8;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(16, 32);
            this.pictureBox10.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(124, 124);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 97;
            this.pictureBox10.TabStop = false;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.White;
            this.groupBox5.Controls.Add(this.nudProductAmount);
            this.groupBox5.Controls.Add(this.cbbFloor);
            this.groupBox5.Controls.Add(this.lblNumberItems);
            this.groupBox5.Controls.Add(this.pictureBox8);
            this.groupBox5.Controls.Add(this.metroLabel2);
            this.groupBox5.Controls.Add(this.lblFloor);
            this.groupBox5.Controls.Add(this.btnOrder);
            this.groupBox5.Location = new System.Drawing.Point(649, 17);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox5.Size = new System.Drawing.Size(588, 133);
            this.groupBox5.TabIndex = 96;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Place restock request";
            // 
            // nudProductAmount
            // 
            this.nudProductAmount.Location = new System.Drawing.Point(266, 67);
            this.nudProductAmount.Name = "nudProductAmount";
            this.nudProductAmount.Size = new System.Drawing.Size(124, 20);
            this.nudProductAmount.TabIndex = 97;
            // 
            // cbbFloor
            // 
            this.cbbFloor.FormattingEnabled = true;
            this.cbbFloor.Location = new System.Drawing.Point(76, 98);
            this.cbbFloor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbbFloor.Name = "cbbFloor";
            this.cbbFloor.Size = new System.Drawing.Size(92, 21);
            this.cbbFloor.TabIndex = 96;
            // 
            // lblNumberItems
            // 
            this.lblNumberItems.AutoSize = true;
            this.lblNumberItems.Location = new System.Drawing.Point(14, 69);
            this.lblNumberItems.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNumberItems.Name = "lblNumberItems";
            this.lblNumberItems.Size = new System.Drawing.Size(253, 20);
            this.lblNumberItems.TabIndex = 63;
            this.lblNumberItems.Text = "Amount of items need to be requested:";
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.White;
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(11, 22);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(35, 36);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 95;
            this.pictureBox8.TabStop = false;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(65, 22);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(160, 20);
            this.metroLabel2.TabIndex = 65;
            this.metroLabel2.Text = "Select an item please!";
            // 
            // lblFloor
            // 
            this.lblFloor.AutoSize = true;
            this.lblFloor.Location = new System.Drawing.Point(14, 98);
            this.lblFloor.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFloor.Name = "lblFloor";
            this.lblFloor.Size = new System.Drawing.Size(44, 20);
            this.lblFloor.TabIndex = 66;
            this.lblFloor.Text = "Floor:";
            // 
            // btnOrder
            // 
            this.btnOrder.BackColor = System.Drawing.Color.DarkCyan;
            this.btnOrder.Location = new System.Drawing.Point(244, 92);
            this.btnOrder.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnOrder.Name = "btnOrder";
            this.btnOrder.Size = new System.Drawing.Size(171, 34);
            this.btnOrder.TabIndex = 69;
            this.btnOrder.Text = "Order";
            this.btnOrder.UseVisualStyleBackColor = false;
            this.btnOrder.Click += new System.EventHandler(this.btnRestockRequest_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.pictureBox4);
            this.groupBox3.Controls.Add(this.metroLabel1);
            this.groupBox3.Controls.Add(this.tbProductSearchInput);
            this.groupBox3.Controls.Add(this.btnSearchForItem);
            this.groupBox3.Location = new System.Drawing.Point(180, 23);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Size = new System.Drawing.Size(367, 133);
            this.groupBox3.TabIndex = 94;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Search for an item";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(9, 23);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(39, 39);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 83;
            this.pictureBox4.TabStop = false;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.Location = new System.Drawing.Point(110, 33);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(46, 25);
            this.metroLabel1.TabIndex = 81;
            this.metroLabel1.Text = "Info:";
            // 
            // tbProductSearchInput
            // 
            this.tbProductSearchInput.Location = new System.Drawing.Point(174, 33);
            this.tbProductSearchInput.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbProductSearchInput.Name = "tbProductSearchInput";
            this.tbProductSearchInput.Size = new System.Drawing.Size(131, 19);
            this.tbProductSearchInput.TabIndex = 2;
            // 
            // btnSearchForItem
            // 
            this.btnSearchForItem.BackColor = System.Drawing.Color.DarkCyan;
            this.btnSearchForItem.Location = new System.Drawing.Point(110, 80);
            this.btnSearchForItem.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSearchForItem.Name = "btnSearchForItem";
            this.btnSearchForItem.Size = new System.Drawing.Size(196, 40);
            this.btnSearchForItem.TabIndex = 62;
            this.btnSearchForItem.Text = "Search Item";
            this.btnSearchForItem.UseVisualStyleBackColor = false;
            this.btnSearchForItem.Click += new System.EventHandler(this.btnSearchForItem_Click);
            // 
            // dgvProducts
            // 
            this.dgvProducts.AllowUserToAddRows = false;
            this.dgvProducts.AllowUserToDeleteRows = false;
            this.dgvProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.barcode,
            this.name,
            this.modelNumber,
            this.manufacturerBrand,
            this.category,
            this.normalPrice,
            this.promotionPrice,
            this.stockInDepot,
            this.stockOnShelves,
            this.sold});
            this.dgvProducts.Location = new System.Drawing.Point(16, 168);
            this.dgvProducts.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvProducts.Name = "dgvProducts";
            this.dgvProducts.ReadOnly = true;
            this.dgvProducts.RowHeadersWidth = 51;
            this.dgvProducts.RowTemplate.Height = 24;
            this.dgvProducts.Size = new System.Drawing.Size(1221, 197);
            this.dgvProducts.TabIndex = 92;
            // 
            // barcode
            // 
            this.barcode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.barcode.HeaderText = "Barcode:";
            this.barcode.MinimumWidth = 6;
            this.barcode.Name = "barcode";
            this.barcode.ReadOnly = true;
            this.barcode.Width = 85;
            // 
            // name
            // 
            this.name.HeaderText = "Name:";
            this.name.MinimumWidth = 6;
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Width = 125;
            // 
            // modelNumber
            // 
            this.modelNumber.HeaderText = "Model number:";
            this.modelNumber.MinimumWidth = 6;
            this.modelNumber.Name = "modelNumber";
            this.modelNumber.ReadOnly = true;
            this.modelNumber.Width = 125;
            // 
            // manufacturerBrand
            // 
            this.manufacturerBrand.HeaderText = "Brand:";
            this.manufacturerBrand.MinimumWidth = 6;
            this.manufacturerBrand.Name = "manufacturerBrand";
            this.manufacturerBrand.ReadOnly = true;
            this.manufacturerBrand.Width = 125;
            // 
            // category
            // 
            this.category.HeaderText = "Category:";
            this.category.MinimumWidth = 6;
            this.category.Name = "category";
            this.category.ReadOnly = true;
            this.category.Width = 125;
            // 
            // normalPrice
            // 
            this.normalPrice.HeaderText = "Normal price:";
            this.normalPrice.MinimumWidth = 6;
            this.normalPrice.Name = "normalPrice";
            this.normalPrice.ReadOnly = true;
            this.normalPrice.Width = 125;
            // 
            // promotionPrice
            // 
            this.promotionPrice.HeaderText = "Promotion price:";
            this.promotionPrice.MinimumWidth = 6;
            this.promotionPrice.Name = "promotionPrice";
            this.promotionPrice.ReadOnly = true;
            this.promotionPrice.Width = 125;
            // 
            // stockInDepot
            // 
            this.stockInDepot.HeaderText = "In depot:";
            this.stockInDepot.MinimumWidth = 6;
            this.stockInDepot.Name = "stockInDepot";
            this.stockInDepot.ReadOnly = true;
            this.stockInDepot.Width = 125;
            // 
            // stockOnShelves
            // 
            this.stockOnShelves.HeaderText = "On shelves:";
            this.stockOnShelves.MinimumWidth = 6;
            this.stockOnShelves.Name = "stockOnShelves";
            this.stockOnShelves.ReadOnly = true;
            this.stockOnShelves.Width = 125;
            // 
            // sold
            // 
            this.sold.HeaderText = "Amount sold:";
            this.sold.MinimumWidth = 6;
            this.sold.Name = "sold";
            this.sold.ReadOnly = true;
            this.sold.Width = 125;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.btnLogOut);
            this.panel1.Controls.Add(this.lbLoggedInAs);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1281, 96);
            this.panel1.TabIndex = 4;
            // 
            // btnLogOut
            // 
            this.btnLogOut.BackColor = System.Drawing.Color.Black;
            this.btnLogOut.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogOut.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnLogOut.Location = new System.Drawing.Point(1198, 8);
            this.btnLogOut.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(71, 42);
            this.btnLogOut.TabIndex = 1;
            this.btnLogOut.Text = "Log Out";
            this.btnLogOut.UseVisualStyleBackColor = false;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // lbLoggedInAs
            // 
            this.lbLoggedInAs.AutoSize = true;
            this.lbLoggedInAs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoggedInAs.Location = new System.Drawing.Point(248, 37);
            this.lbLoggedInAs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbLoggedInAs.Name = "lbLoggedInAs";
            this.lbLoggedInAs.Size = new System.Drawing.Size(129, 40);
            this.lbLoggedInAs.TabIndex = 4;
            this.lbLoggedInAs.Text = "Logged in as: \r\n\r\n";
            // 
            // cbbModifyContractFte
            // 
            this.cbbModifyContractFte.FormattingEnabled = true;
            this.cbbModifyContractFte.Location = new System.Drawing.Point(115, 90);
            this.cbbModifyContractFte.Margin = new System.Windows.Forms.Padding(2);
            this.cbbModifyContractFte.Name = "cbbModifyContractFte";
            this.cbbModifyContractFte.Size = new System.Drawing.Size(98, 21);
            this.cbbModifyContractFte.TabIndex = 106;
            // 
            // frmManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 587);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tcManager);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frmManager";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Manager Application";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tcManager.ResumeLayout(false);
            this.ManageEmployeePage.ResumeLayout(false);
            this.metroTabControl2.ResumeLayout(false);
            this.AddEmployeePage.ResumeLayout(false);
            this.AddEmployeePage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.EmployeeContractPage.ResumeLayout(false);
            this.EmployeeContractPage.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHourlySalary)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContracts)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudNewHourlySalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.gbSearch.ResumeLayout(false);
            this.gbSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployees)).EndInit();
            this.StatisticsPage.ResumeLayout(false);
            this.StatisticsPage.PerformLayout();
            this.gbCompanyStatistics.ResumeLayout(false);
            this.gbCompanyStatistics.PerformLayout();
            this.gbStatistics.ResumeLayout(false);
            this.gbStatistics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStatistics)).EndInit();
            this.RestockRequestPage.ResumeLayout(false);
            this.metroTabControl3.ResumeLayout(false);
            this.placeRestockRequestPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudProductAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProducts)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroTabControl tcManager;
        private MetroFramework.Controls.MetroTabPage ManageEmployeePage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.Label lbLoggedInAs;
        private MetroFramework.Controls.MetroTabPage RestockRequestPage;
        private MetroFramework.Controls.MetroTabControl metroTabControl2;
        private MetroFramework.Controls.MetroTabPage AddEmployeePage;
        private System.Windows.Forms.Button btnAddEmployee;
        private MetroFramework.Controls.MetroTextBox tbEmergencyPhoneNumber;
        private MetroFramework.Controls.MetroTextBox tbPhoneNumber;
        private MetroFramework.Controls.MetroTextBox tbNationality;
        private MetroFramework.Controls.MetroTextBox tbEmail;
        private MetroFramework.Controls.MetroLabel lblEmail;
        private MetroFramework.Controls.MetroLabel lblGender;
        private System.Windows.Forms.DateTimePicker dtpBirthday;
        private MetroFramework.Controls.MetroLabel lblEmergency;
        private MetroFramework.Controls.MetroLabel lblPhoneNumber;
        private MetroFramework.Controls.MetroLabel lblRegion;
        private MetroFramework.Controls.MetroLabel lblNationality;
        private MetroFramework.Controls.MetroTextBox tbAddress;
        private MetroFramework.Controls.MetroTextBox tbBSN;
        private MetroFramework.Controls.MetroTextBox tbLastName;
        private MetroFramework.Controls.MetroTextBox tbFirstName;
        private MetroFramework.Controls.MetroLabel lblBirthday;
        private MetroFramework.Controls.MetroLabel lblBSN;
        private MetroFramework.Controls.MetroLabel lblLastName;
        private MetroFramework.Controls.MetroLabel lblEmployeeName;
        private MetroFramework.Controls.MetroTabControl metroTabControl3;
        private MetroFramework.Controls.MetroTabPage placeRestockRequestPage;
        private MetroFramework.Controls.MetroLabel lblFloor;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel lblNumberItems;
        private System.Windows.Forms.Button btnSearchForItem;
        private MetroFramework.Controls.MetroTextBox tbProductSearchInput;
        private System.Windows.Forms.Button btnOrder;
        private MetroFramework.Controls.MetroTabPage EmployeeContractPage;
        private System.Windows.Forms.Button btnSearchForEmployee;
        private MetroFramework.Controls.MetroTextBox tbSearchInput;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private System.Windows.Forms.DataGridView dgvEmployees;
        private System.Windows.Forms.GroupBox gbSearch;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private MetroFramework.Controls.MetroLabel lblSalary;
        private MetroFramework.Controls.MetroLabel lblnewPosition;
        private System.Windows.Forms.Button btnEditContract;
        private System.Windows.Forms.GroupBox groupBox4;
        private MetroFramework.Controls.MetroLabel cbDepartureType;
        private MetroFramework.Controls.MetroLabel lblReason;
        private MetroFramework.Controls.MetroTextBox tbDepartureReason;
        private System.Windows.Forms.Button btnEndContract;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.DataGridView dgvProducts;
        private System.Windows.Forms.ComboBox cbbPositionEdit;
        private System.Windows.Forms.ComboBox cbbFloor;
        private MetroFramework.Controls.MetroLabel lblZipcode;
        private MetroFramework.Controls.MetroLabel lblStreetName;
        private MetroFramework.Controls.MetroTextBox tbStreetName;
        private MetroFramework.Controls.MetroTextBox tbZipcode;
        private System.Windows.Forms.CheckedListBox clbLanguages;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gender;
        private System.Windows.Forms.DataGridViewTextBoxColumn bsn;
        private System.Windows.Forms.DataGridViewTextBoxColumn birthDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn nationality;
        private System.Windows.Forms.DataGridViewTextBoxColumn spokenLanguages;
        private System.Windows.Forms.DataGridViewTextBoxColumn region;
        private System.Windows.Forms.DataGridViewTextBoxColumn zipCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn streetName;
        private System.Windows.Forms.DataGridViewTextBoxColumn phoneNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn emergencyPhoneNumber;
        private System.Windows.Forms.ComboBox cbGender;
        private System.Windows.Forms.DataGridView dgvContracts;
        private MetroFramework.Controls.MetroRadioButton rbSearchContract;
        private MetroFramework.Controls.MetroRadioButton rbSearchEmployee;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpEndContractEndDate;
        private System.Windows.Forms.NumericUpDown nudNewHourlySalary;
        private System.Windows.Forms.ComboBox cbbDepartureType;
        private System.Windows.Forms.NumericUpDown nudProductAmount;
        private MetroFramework.Controls.MetroCheckBox cbSpouse;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Button btnRenewContract;
        private System.Windows.Forms.PictureBox pictureBox10;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox7;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.NumericUpDown nudHourlySalary;
        private System.Windows.Forms.ComboBox cbbAddContractPosition;
        private MetroFramework.Controls.MetroCheckBox cbEndDate;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel lblPosition;
        private System.Windows.Forms.Button btnAddContract;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private System.Windows.Forms.DateTimePicker EndDateAddNewContract;
        private System.Windows.Forms.DateTimePicker dtpStartDatedtp;
        private System.Windows.Forms.DateTimePicker dtpEndDateRenewContract;
        private System.Windows.Forms.ComboBox cmbFTE;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private System.Windows.Forms.DataGridViewTextBoxColumn contractId;
        private System.Windows.Forms.DataGridViewTextBoxColumn contractPosition;
        private System.Windows.Forms.DataGridViewTextBoxColumn hourlySalary;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn endDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn FTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn active;
        private System.Windows.Forms.DataGridViewTextBoxColumn departureReason;
        private System.Windows.Forms.DataGridViewTextBoxColumn departureType;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroTabPage StatisticsPage;
        private System.Windows.Forms.GroupBox gbCompanyStatistics;
        private MetroFramework.Controls.MetroTextBox tbxTotalActiveEmployees;
        private MetroFramework.Controls.MetroLabel lblTotalEmployees;
        private System.Windows.Forms.GroupBox gbStatistics;
        private MetroFramework.Controls.MetroTextBox tbxActiveEmployeesDepartment;
        private MetroFramework.Controls.MetroTextBox tbxRequiredFte;
        private MetroFramework.Controls.MetroTextBox tbxAvailableFte;
        private MetroFramework.Controls.MetroLabel lblRequiredFte;
        private MetroFramework.Controls.MetroLabel lblAmountOfActiveEmployees;
        private MetroFramework.Controls.MetroLabel lblAvailableFte;
        private MetroFramework.Controls.MetroLabel lblDepartmentStatistics;
        private System.Windows.Forms.ComboBox cbbDepartmentStatistics;
        private System.Windows.Forms.DataGridView dgvStatistics;
        private System.Windows.Forms.DataGridViewTextBoxColumn statisticsName;
        private System.Windows.Forms.DataGridViewTextBoxColumn statisticsFte;
        private System.Windows.Forms.DataGridViewTextBoxColumn barcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn manufacturerBrand;
        private System.Windows.Forms.DataGridViewTextBoxColumn category;
        private System.Windows.Forms.DataGridViewTextBoxColumn normalPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn promotionPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockInDepot;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockOnShelves;
        private System.Windows.Forms.DataGridViewTextBoxColumn sold;
        private System.Windows.Forms.ComboBox cbbModifyContractFte;
    }
}