﻿namespace GroupProject_MediaBazzar
{
    partial class frmAdministration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAdministration));
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.tabPageAssign = new MetroFramework.Controls.MetroTabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnOverview = new System.Windows.Forms.Button();
            this.gbAssignEmployee = new System.Windows.Forms.GroupBox();
            this.cbbFloor = new MetroFramework.Controls.MetroComboBox();
            this.lbSelectedEmployees = new System.Windows.Forms.ListBox();
            this.btnAssignEmployee = new System.Windows.Forms.Button();
            this.mcbSelectMultiple = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.dtpShiftDate = new System.Windows.Forms.DateTimePicker();
            this.rbMorning = new MetroFramework.Controls.MetroRadioButton();
            this.rbEvening = new MetroFramework.Controls.MetroRadioButton();
            this.rbAfternoon = new MetroFramework.Controls.MetroRadioButton();
            this.dgvEmployeeInformation = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FTE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remainingFte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtpDepartments = new MetroFramework.Controls.MetroTabPage();
            this.btnDeleteDepartment = new System.Windows.Forms.Button();
            this.btnEditDepartment = new System.Windows.Forms.Button();
            this.btnAddDepartment = new System.Windows.Forms.Button();
            this.nudRequiredEmployees = new System.Windows.Forms.NumericUpDown();
            this.tbDepartmentName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbDepartmentName = new System.Windows.Forms.Label();
            this.lbPositionSelect = new System.Windows.Forms.Label();
            this.cbbPosition = new System.Windows.Forms.ComboBox();
            this.lbxDepartments = new System.Windows.Forms.ListBox();
            this.tabpageAutoScheduler = new MetroFramework.Controls.MetroTabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtpAutoScheduler = new System.Windows.Forms.DateTimePicker();
            this.btnGenerateAutoSchedule = new System.Windows.Forms.Button();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.dgvTemplateView = new System.Windows.Forms.DataGridView();
            this.tabpageModifyEmployeedetails = new MetroFramework.Controls.MetroTabPage();
            this.dgvEditEmployeeDetails = new System.Windows.Forms.DataGridView();
            this.employeeid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BSN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.tbInput = new MetroFramework.Controls.MetroTextBox();
            this.btnSearchEmployee = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.btnEditEmplyeeData = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.lbLoggedInAs = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroTabControl1.SuspendLayout();
            this.tabPageAssign.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.gbAssignEmployee.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeeInformation)).BeginInit();
            this.mtpDepartments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRequiredEmployees)).BeginInit();
            this.tabpageAutoScheduler.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTemplateView)).BeginInit();
            this.tabpageModifyEmployeedetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEditEmployeeDetails)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.tabPageAssign);
            this.metroTabControl1.Controls.Add(this.tabpageAutoScheduler);
            this.metroTabControl1.Controls.Add(this.mtpDepartments);
            this.metroTabControl1.Controls.Add(this.tabpageModifyEmployeedetails);
            this.metroTabControl1.CustomBackground = true;
            this.metroTabControl1.ItemSize = new System.Drawing.Size(400, 70);
            this.metroTabControl1.Location = new System.Drawing.Point(0, 127);
            this.metroTabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 2;
            this.metroTabControl1.Size = new System.Drawing.Size(1279, 658);
            this.metroTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroTabControl1.TabIndex = 0;
            // 
            // tabPageAssign
            // 
            this.tabPageAssign.Controls.Add(this.groupBox3);
            this.tabPageAssign.Controls.Add(this.gbAssignEmployee);
            this.tabPageAssign.Controls.Add(this.dgvEmployeeInformation);
            this.tabPageAssign.HorizontalScrollbarBarColor = true;
            this.tabPageAssign.Location = new System.Drawing.Point(4, 74);
            this.tabPageAssign.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPageAssign.Name = "tabPageAssign";
            this.tabPageAssign.Size = new System.Drawing.Size(1271, 580);
            this.tabPageAssign.TabIndex = 1;
            this.tabPageAssign.Text = "Employee";
            this.tabPageAssign.VerticalScrollbarBarColor = true;
            this.tabPageAssign.VerticalScrollbarSize = 11;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.White;
            this.groupBox3.Controls.Add(this.btnOverview);
            this.groupBox3.Location = new System.Drawing.Point(821, 48);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(419, 142);
            this.groupBox3.TabIndex = 108;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Shift Management";
            // 
            // btnOverview
            // 
            this.btnOverview.BackColor = System.Drawing.Color.DarkCyan;
            this.btnOverview.Location = new System.Drawing.Point(103, 38);
            this.btnOverview.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOverview.Name = "btnOverview";
            this.btnOverview.Size = new System.Drawing.Size(201, 68);
            this.btnOverview.TabIndex = 91;
            this.btnOverview.Text = "View Shifts";
            this.btnOverview.UseVisualStyleBackColor = false;
            this.btnOverview.Click += new System.EventHandler(this.btnOverview_Click);
            // 
            // gbAssignEmployee
            // 
            this.gbAssignEmployee.BackColor = System.Drawing.Color.White;
            this.gbAssignEmployee.Controls.Add(this.cbbFloor);
            this.gbAssignEmployee.Controls.Add(this.lbSelectedEmployees);
            this.gbAssignEmployee.Controls.Add(this.btnAssignEmployee);
            this.gbAssignEmployee.Controls.Add(this.mcbSelectMultiple);
            this.gbAssignEmployee.Controls.Add(this.metroLabel2);
            this.gbAssignEmployee.Controls.Add(this.metroLabel1);
            this.gbAssignEmployee.Controls.Add(this.dtpShiftDate);
            this.gbAssignEmployee.Controls.Add(this.rbMorning);
            this.gbAssignEmployee.Controls.Add(this.rbEvening);
            this.gbAssignEmployee.Controls.Add(this.rbAfternoon);
            this.gbAssignEmployee.Location = new System.Drawing.Point(37, 2);
            this.gbAssignEmployee.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbAssignEmployee.Name = "gbAssignEmployee";
            this.gbAssignEmployee.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbAssignEmployee.Size = new System.Drawing.Size(736, 226);
            this.gbAssignEmployee.TabIndex = 107;
            this.gbAssignEmployee.TabStop = false;
            this.gbAssignEmployee.Text = "Assign Employee to a shift";
            // 
            // cbbFloor
            // 
            this.cbbFloor.FormattingEnabled = true;
            this.cbbFloor.ItemHeight = 24;
            this.cbbFloor.Location = new System.Drawing.Point(64, 66);
            this.cbbFloor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbbFloor.Name = "cbbFloor";
            this.cbbFloor.Size = new System.Drawing.Size(176, 30);
            this.cbbFloor.TabIndex = 90;
            this.cbbFloor.SelectedIndexChanged += new System.EventHandler(this.cbbFloor_SelectedIndexChanged);
            // 
            // lbSelectedEmployees
            // 
            this.lbSelectedEmployees.FormattingEnabled = true;
            this.lbSelectedEmployees.ItemHeight = 16;
            this.lbSelectedEmployees.Location = new System.Drawing.Point(279, 102);
            this.lbSelectedEmployees.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lbSelectedEmployees.Name = "lbSelectedEmployees";
            this.lbSelectedEmployees.Size = new System.Drawing.Size(287, 100);
            this.lbSelectedEmployees.TabIndex = 89;
            // 
            // btnAssignEmployee
            // 
            this.btnAssignEmployee.BackColor = System.Drawing.Color.DarkCyan;
            this.btnAssignEmployee.Location = new System.Drawing.Point(601, 101);
            this.btnAssignEmployee.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAssignEmployee.Name = "btnAssignEmployee";
            this.btnAssignEmployee.Size = new System.Drawing.Size(95, 101);
            this.btnAssignEmployee.TabIndex = 88;
            this.btnAssignEmployee.Text = "Assign Employee\r\n";
            this.btnAssignEmployee.UseVisualStyleBackColor = false;
            this.btnAssignEmployee.Click += new System.EventHandler(this.btnAssignEmployee_Click);
            // 
            // mcbSelectMultiple
            // 
            this.mcbSelectMultiple.AutoSize = true;
            this.mcbSelectMultiple.Location = new System.Drawing.Point(279, 66);
            this.mcbSelectMultiple.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.mcbSelectMultiple.Name = "mcbSelectMultiple";
            this.mcbSelectMultiple.Size = new System.Drawing.Size(332, 17);
            this.mcbSelectMultiple.TabIndex = 0;
            this.mcbSelectMultiple.Text = "Select Multiple Employee to assign them to the shift?";
            this.mcbSelectMultiple.UseVisualStyleBackColor = true;
            this.mcbSelectMultiple.CheckedChanged += new System.EventHandler(this.mcbSelMult_CheckedChanged_1);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(13, 102);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(36, 20);
            this.metroLabel2.TabIndex = 12;
            this.metroLabel2.Text = "Day:";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(13, 66);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(44, 20);
            this.metroLabel1.TabIndex = 11;
            this.metroLabel1.Text = "Floor:";
            // 
            // dtpShiftDate
            // 
            this.dtpShiftDate.Location = new System.Drawing.Point(64, 102);
            this.dtpShiftDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtpShiftDate.Name = "dtpShiftDate";
            this.dtpShiftDate.Size = new System.Drawing.Size(176, 22);
            this.dtpShiftDate.TabIndex = 16;
            this.dtpShiftDate.ValueChanged += new System.EventHandler(this.DtpShiftDate_ValueChanged);
            // 
            // rbMorning
            // 
            this.rbMorning.AutoSize = true;
            this.rbMorning.Location = new System.Drawing.Point(283, 30);
            this.rbMorning.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbMorning.Name = "rbMorning";
            this.rbMorning.Size = new System.Drawing.Size(74, 17);
            this.rbMorning.TabIndex = 82;
            this.rbMorning.TabStop = true;
            this.rbMorning.Text = "Morning";
            this.rbMorning.UseVisualStyleBackColor = true;
            this.rbMorning.CheckedChanged += new System.EventHandler(this.RbMorning_CheckedChanged);
            // 
            // rbEvening
            // 
            this.rbEvening.AutoSize = true;
            this.rbEvening.Location = new System.Drawing.Point(541, 30);
            this.rbEvening.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbEvening.Name = "rbEvening";
            this.rbEvening.Size = new System.Drawing.Size(69, 17);
            this.rbEvening.TabIndex = 84;
            this.rbEvening.TabStop = true;
            this.rbEvening.Text = "Evening";
            this.rbEvening.UseVisualStyleBackColor = true;
            this.rbEvening.CheckedChanged += new System.EventHandler(this.RbEvening_CheckedChanged);
            // 
            // rbAfternoon
            // 
            this.rbAfternoon.AutoSize = true;
            this.rbAfternoon.Location = new System.Drawing.Point(412, 30);
            this.rbAfternoon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbAfternoon.Name = "rbAfternoon";
            this.rbAfternoon.Size = new System.Drawing.Size(82, 17);
            this.rbAfternoon.TabIndex = 83;
            this.rbAfternoon.TabStop = true;
            this.rbAfternoon.Text = "Afternoon";
            this.rbAfternoon.UseVisualStyleBackColor = true;
            this.rbAfternoon.CheckedChanged += new System.EventHandler(this.RbAfternoon_CheckedChanged);
            // 
            // dgvEmployeeInformation
            // 
            this.dgvEmployeeInformation.AllowUserToAddRows = false;
            this.dgvEmployeeInformation.AllowUserToDeleteRows = false;
            this.dgvEmployeeInformation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEmployeeInformation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployeeInformation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.firstName,
            this.lastName,
            this.FTE,
            this.remainingFte});
            this.dgvEmployeeInformation.Location = new System.Drawing.Point(37, 254);
            this.dgvEmployeeInformation.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvEmployeeInformation.Name = "dgvEmployeeInformation";
            this.dgvEmployeeInformation.ReadOnly = true;
            this.dgvEmployeeInformation.RowHeadersWidth = 51;
            this.dgvEmployeeInformation.RowTemplate.Height = 24;
            this.dgvEmployeeInformation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEmployeeInformation.Size = new System.Drawing.Size(1203, 330);
            this.dgvEmployeeInformation.TabIndex = 92;
            this.dgvEmployeeInformation.DoubleClick += new System.EventHandler(this.dgvEmployeeInformation_DoubleClick);
            // 
            // id
            // 
            this.id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.id.FillWeight = 106.9519F;
            this.id.HeaderText = "Id:";
            this.id.MinimumWidth = 6;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Width = 52;
            // 
            // firstName
            // 
            this.firstName.FillWeight = 97.6827F;
            this.firstName.HeaderText = "First name:";
            this.firstName.MinimumWidth = 6;
            this.firstName.Name = "firstName";
            this.firstName.ReadOnly = true;
            // 
            // lastName
            // 
            this.lastName.FillWeight = 97.6827F;
            this.lastName.HeaderText = "Last name:";
            this.lastName.MinimumWidth = 6;
            this.lastName.Name = "lastName";
            this.lastName.ReadOnly = true;
            // 
            // FTE
            // 
            this.FTE.FillWeight = 97.6827F;
            this.FTE.HeaderText = "FTE:";
            this.FTE.MinimumWidth = 6;
            this.FTE.Name = "FTE";
            this.FTE.ReadOnly = true;
            // 
            // remainingFte
            // 
            this.remainingFte.HeaderText = "Remaining FTE:";
            this.remainingFte.MinimumWidth = 6;
            this.remainingFte.Name = "remainingFte";
            this.remainingFte.ReadOnly = true;
            // 
            // mtpDepartments
            // 
            this.mtpDepartments.Controls.Add(this.btnDeleteDepartment);
            this.mtpDepartments.Controls.Add(this.btnEditDepartment);
            this.mtpDepartments.Controls.Add(this.btnAddDepartment);
            this.mtpDepartments.Controls.Add(this.nudRequiredEmployees);
            this.mtpDepartments.Controls.Add(this.tbDepartmentName);
            this.mtpDepartments.Controls.Add(this.label1);
            this.mtpDepartments.Controls.Add(this.lbDepartmentName);
            this.mtpDepartments.Controls.Add(this.lbPositionSelect);
            this.mtpDepartments.Controls.Add(this.cbbPosition);
            this.mtpDepartments.Controls.Add(this.lbxDepartments);
            this.mtpDepartments.HorizontalScrollbarBarColor = true;
            this.mtpDepartments.HorizontalScrollbarSize = 12;
            this.mtpDepartments.Location = new System.Drawing.Point(4, 74);
            this.mtpDepartments.Margin = new System.Windows.Forms.Padding(4);
            this.mtpDepartments.Name = "mtpDepartments";
            this.mtpDepartments.Size = new System.Drawing.Size(1271, 580);
            this.mtpDepartments.TabIndex = 5;
            this.mtpDepartments.Text = "Departments";
            this.mtpDepartments.VerticalScrollbarBarColor = true;
            this.mtpDepartments.VerticalScrollbarSize = 13;
            // 
            // btnDeleteDepartment
            // 
            this.btnDeleteDepartment.BackColor = System.Drawing.Color.DarkCyan;
            this.btnDeleteDepartment.Location = new System.Drawing.Point(883, 314);
            this.btnDeleteDepartment.Margin = new System.Windows.Forms.Padding(4);
            this.btnDeleteDepartment.Name = "btnDeleteDepartment";
            this.btnDeleteDepartment.Size = new System.Drawing.Size(379, 47);
            this.btnDeleteDepartment.TabIndex = 11;
            this.btnDeleteDepartment.Text = "Delete department";
            this.btnDeleteDepartment.UseVisualStyleBackColor = false;
            this.btnDeleteDepartment.Click += new System.EventHandler(this.btnDeleteDepartment_Click);
            // 
            // btnEditDepartment
            // 
            this.btnEditDepartment.BackColor = System.Drawing.Color.DarkCyan;
            this.btnEditDepartment.Location = new System.Drawing.Point(883, 239);
            this.btnEditDepartment.Margin = new System.Windows.Forms.Padding(4);
            this.btnEditDepartment.Name = "btnEditDepartment";
            this.btnEditDepartment.Size = new System.Drawing.Size(379, 47);
            this.btnEditDepartment.TabIndex = 10;
            this.btnEditDepartment.Text = "Edit department";
            this.btnEditDepartment.UseVisualStyleBackColor = false;
            this.btnEditDepartment.Click += new System.EventHandler(this.btnEditDepartment_Click);
            // 
            // btnAddDepartment
            // 
            this.btnAddDepartment.BackColor = System.Drawing.Color.DarkCyan;
            this.btnAddDepartment.ForeColor = System.Drawing.Color.Black;
            this.btnAddDepartment.Location = new System.Drawing.Point(880, 158);
            this.btnAddDepartment.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddDepartment.Name = "btnAddDepartment";
            this.btnAddDepartment.Size = new System.Drawing.Size(379, 47);
            this.btnAddDepartment.TabIndex = 9;
            this.btnAddDepartment.Text = "Add department";
            this.btnAddDepartment.UseVisualStyleBackColor = false;
            this.btnAddDepartment.Click += new System.EventHandler(this.btnAddDepartment_Click);
            // 
            // nudRequiredEmployees
            // 
            this.nudRequiredEmployees.Location = new System.Drawing.Point(1193, 101);
            this.nudRequiredEmployees.Margin = new System.Windows.Forms.Padding(4);
            this.nudRequiredEmployees.Name = "nudRequiredEmployees";
            this.nudRequiredEmployees.Size = new System.Drawing.Size(69, 22);
            this.nudRequiredEmployees.TabIndex = 8;
            // 
            // tbDepartmentName
            // 
            this.tbDepartmentName.Location = new System.Drawing.Point(1059, 61);
            this.tbDepartmentName.Margin = new System.Windows.Forms.Padding(4);
            this.tbDepartmentName.Name = "tbDepartmentName";
            this.tbDepartmentName.Size = new System.Drawing.Size(200, 22);
            this.tbDepartmentName.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(874, 101);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(296, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "Expected required shifts per week";
            // 
            // lbDepartmentName
            // 
            this.lbDepartmentName.AutoSize = true;
            this.lbDepartmentName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDepartmentName.Location = new System.Drawing.Point(874, 61);
            this.lbDepartmentName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbDepartmentName.Name = "lbDepartmentName";
            this.lbDepartmentName.Size = new System.Drawing.Size(165, 24);
            this.lbDepartmentName.TabIndex = 5;
            this.lbDepartmentName.Text = "Department name:";
            // 
            // lbPositionSelect
            // 
            this.lbPositionSelect.AutoSize = true;
            this.lbPositionSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPositionSelect.Location = new System.Drawing.Point(875, 20);
            this.lbPositionSelect.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPositionSelect.Name = "lbPositionSelect";
            this.lbPositionSelect.Size = new System.Drawing.Size(137, 24);
            this.lbPositionSelect.TabIndex = 4;
            this.lbPositionSelect.Text = "Select position:";
            // 
            // cbbPosition
            // 
            this.cbbPosition.FormattingEnabled = true;
            this.cbbPosition.Location = new System.Drawing.Point(1031, 20);
            this.cbbPosition.Margin = new System.Windows.Forms.Padding(4);
            this.cbbPosition.Name = "cbbPosition";
            this.cbbPosition.Size = new System.Drawing.Size(228, 24);
            this.cbbPosition.TabIndex = 3;
            // 
            // lbxDepartments
            // 
            this.lbxDepartments.FormattingEnabled = true;
            this.lbxDepartments.ItemHeight = 16;
            this.lbxDepartments.Location = new System.Drawing.Point(11, 16);
            this.lbxDepartments.Margin = new System.Windows.Forms.Padding(4);
            this.lbxDepartments.Name = "lbxDepartments";
            this.lbxDepartments.Size = new System.Drawing.Size(855, 532);
            this.lbxDepartments.TabIndex = 2;
            this.lbxDepartments.SelectedIndexChanged += new System.EventHandler(this.lbxDepartments_SelectedIndexChanged);
            // 
            // tabpageAutoScheduler
            // 
            this.tabpageAutoScheduler.Controls.Add(this.groupBox2);
            this.tabpageAutoScheduler.Controls.Add(this.dgvTemplateView);
            this.tabpageAutoScheduler.HorizontalScrollbarBarColor = true;
            this.tabpageAutoScheduler.Location = new System.Drawing.Point(4, 74);
            this.tabpageAutoScheduler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabpageAutoScheduler.Name = "tabpageAutoScheduler";
            this.tabpageAutoScheduler.Size = new System.Drawing.Size(1271, 580);
            this.tabpageAutoScheduler.TabIndex = 4;
            this.tabpageAutoScheduler.Text = "Auto Scheduler";
            this.tabpageAutoScheduler.VerticalScrollbarBarColor = true;
            this.tabpageAutoScheduler.VerticalScrollbarSize = 11;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.dtpAutoScheduler);
            this.groupBox2.Controls.Add(this.btnGenerateAutoSchedule);
            this.groupBox2.Controls.Add(this.metroLabel3);
            this.groupBox2.Location = new System.Drawing.Point(137, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(824, 182);
            this.groupBox2.TabIndex = 95;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Template View";
            // 
            // dtpAutoScheduler
            // 
            this.dtpAutoScheduler.Location = new System.Drawing.Point(249, 77);
            this.dtpAutoScheduler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtpAutoScheduler.Name = "dtpAutoScheduler";
            this.dtpAutoScheduler.Size = new System.Drawing.Size(245, 22);
            this.dtpAutoScheduler.TabIndex = 93;
            // 
            // btnGenerateAutoSchedule
            // 
            this.btnGenerateAutoSchedule.BackColor = System.Drawing.Color.DarkCyan;
            this.btnGenerateAutoSchedule.ForeColor = System.Drawing.Color.Black;
            this.btnGenerateAutoSchedule.Location = new System.Drawing.Point(181, 119);
            this.btnGenerateAutoSchedule.Margin = new System.Windows.Forms.Padding(4);
            this.btnGenerateAutoSchedule.Name = "btnGenerateAutoSchedule";
            this.btnGenerateAutoSchedule.Size = new System.Drawing.Size(379, 47);
            this.btnGenerateAutoSchedule.TabIndex = 94;
            this.btnGenerateAutoSchedule.Text = "Generate  schedule";
            this.btnGenerateAutoSchedule.UseVisualStyleBackColor = false;
            this.btnGenerateAutoSchedule.Click += new System.EventHandler(this.btnGenerateAutoSchedule_Click);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.Location = new System.Drawing.Point(117, 29);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(523, 25);
            this.metroLabel3.TabIndex = 3;
            this.metroLabel3.Text = "Please select first a template to generate the autoschedule";
            // 
            // dgvTemplateView
            // 
            this.dgvTemplateView.AllowUserToAddRows = false;
            this.dgvTemplateView.AllowUserToDeleteRows = false;
            this.dgvTemplateView.AllowUserToResizeColumns = false;
            this.dgvTemplateView.AllowUserToResizeRows = false;
            this.dgvTemplateView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTemplateView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTemplateView.Location = new System.Drawing.Point(45, 235);
            this.dgvTemplateView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvTemplateView.Name = "dgvTemplateView";
            this.dgvTemplateView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgvTemplateView.RowTemplate.Height = 24;
            this.dgvTemplateView.Size = new System.Drawing.Size(1147, 319);
            this.dgvTemplateView.TabIndex = 2;
            // 
            // tabpageModifyEmployeedetails
            // 
            this.tabpageModifyEmployeedetails.Controls.Add(this.dgvEditEmployeeDetails);
            this.tabpageModifyEmployeedetails.Controls.Add(this.groupBox1);
            this.tabpageModifyEmployeedetails.HorizontalScrollbarBarColor = true;
            this.tabpageModifyEmployeedetails.Location = new System.Drawing.Point(4, 74);
            this.tabpageModifyEmployeedetails.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabpageModifyEmployeedetails.Name = "tabpageModifyEmployeedetails";
            this.tabpageModifyEmployeedetails.Size = new System.Drawing.Size(1271, 580);
            this.tabpageModifyEmployeedetails.TabIndex = 3;
            this.tabpageModifyEmployeedetails.Text = "Modify Employee Details";
            this.tabpageModifyEmployeedetails.VerticalScrollbarBarColor = true;
            this.tabpageModifyEmployeedetails.VerticalScrollbarSize = 11;
            // 
            // dgvEditEmployeeDetails
            // 
            this.dgvEditEmployeeDetails.AllowUserToAddRows = false;
            this.dgvEditEmployeeDetails.AllowUserToDeleteRows = false;
            this.dgvEditEmployeeDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEditEmployeeDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEditEmployeeDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.employeeid,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.BSN});
            this.dgvEditEmployeeDetails.Location = new System.Drawing.Point(29, 235);
            this.dgvEditEmployeeDetails.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvEditEmployeeDetails.Name = "dgvEditEmployeeDetails";
            this.dgvEditEmployeeDetails.ReadOnly = true;
            this.dgvEditEmployeeDetails.RowHeadersWidth = 51;
            this.dgvEditEmployeeDetails.RowTemplate.Height = 24;
            this.dgvEditEmployeeDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEditEmployeeDetails.Size = new System.Drawing.Size(1203, 330);
            this.dgvEditEmployeeDetails.TabIndex = 108;
            // 
            // employeeid
            // 
            this.employeeid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.employeeid.FillWeight = 106.9519F;
            this.employeeid.HeaderText = "Id:";
            this.employeeid.MinimumWidth = 6;
            this.employeeid.Name = "employeeid";
            this.employeeid.ReadOnly = true;
            this.employeeid.Width = 52;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.FillWeight = 97.6827F;
            this.dataGridViewTextBoxColumn2.HeaderText = "First name:";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.FillWeight = 97.6827F;
            this.dataGridViewTextBoxColumn3.HeaderText = "Last name:";
            this.dataGridViewTextBoxColumn3.MinimumWidth = 6;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // BSN
            // 
            this.BSN.HeaderText = "BSN";
            this.BSN.MinimumWidth = 6;
            this.BSN.Name = "BSN";
            this.BSN.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.tbInput);
            this.groupBox1.Controls.Add(this.btnSearchEmployee);
            this.groupBox1.Controls.Add(this.pictureBox3);
            this.groupBox1.Controls.Add(this.pictureBox4);
            this.groupBox1.Controls.Add(this.btnEditEmplyeeData);
            this.groupBox1.Location = new System.Drawing.Point(157, 39);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(756, 178);
            this.groupBox1.TabIndex = 107;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Edit Employee details";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel4.Location = new System.Drawing.Point(161, 39);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(46, 20);
            this.metroLabel4.TabIndex = 105;
            this.metroLabel4.Text = "Input:";
            // 
            // tbInput
            // 
            this.tbInput.Location = new System.Drawing.Point(240, 48);
            this.tbInput.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbInput.Name = "tbInput";
            this.tbInput.Size = new System.Drawing.Size(177, 23);
            this.tbInput.TabIndex = 104;
            // 
            // btnSearchEmployee
            // 
            this.btnSearchEmployee.BackColor = System.Drawing.Color.DarkCyan;
            this.btnSearchEmployee.Location = new System.Drawing.Point(452, 26);
            this.btnSearchEmployee.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSearchEmployee.Name = "btnSearchEmployee";
            this.btnSearchEmployee.Size = new System.Drawing.Size(145, 64);
            this.btnSearchEmployee.TabIndex = 77;
            this.btnSearchEmployee.Text = "Search Employee";
            this.btnSearchEmployee.UseVisualStyleBackColor = false;
            this.btnSearchEmployee.Click += new System.EventHandler(this.btnSearchEmployee_Click_1);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(37, 94);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(85, 80);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(37, 32);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(85, 58);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 83;
            this.pictureBox4.TabStop = false;
            // 
            // btnEditEmplyeeData
            // 
            this.btnEditEmplyeeData.BackColor = System.Drawing.Color.DarkCyan;
            this.btnEditEmplyeeData.Location = new System.Drawing.Point(240, 110);
            this.btnEditEmplyeeData.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEditEmplyeeData.Name = "btnEditEmplyeeData";
            this.btnEditEmplyeeData.Size = new System.Drawing.Size(355, 49);
            this.btnEditEmplyeeData.TabIndex = 86;
            this.btnEditEmplyeeData.Text = "Edit Employee";
            this.btnEditEmplyeeData.UseVisualStyleBackColor = false;
            this.btnEditEmplyeeData.Click += new System.EventHandler(this.btnEditEmplyeeData_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.btnLogOut);
            this.panel1.Controls.Add(this.lbLoggedInAs);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(0, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1275, 121);
            this.panel1.TabIndex = 5;
            // 
            // btnLogOut
            // 
            this.btnLogOut.BackColor = System.Drawing.Color.Black;
            this.btnLogOut.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogOut.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnLogOut.Location = new System.Drawing.Point(1168, 11);
            this.btnLogOut.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(95, 52);
            this.btnLogOut.TabIndex = 1;
            this.btnLogOut.Text = "Log Out";
            this.btnLogOut.UseVisualStyleBackColor = false;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // lbLoggedInAs
            // 
            this.lbLoggedInAs.AutoSize = true;
            this.lbLoggedInAs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoggedInAs.Location = new System.Drawing.Point(331, 46);
            this.lbLoggedInAs.Name = "lbLoggedInAs";
            this.lbLoggedInAs.Size = new System.Drawing.Size(129, 40);
            this.lbLoggedInAs.TabIndex = 4;
            this.lbLoggedInAs.Text = "Logged in as: \r\n\r\n";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(203, 121);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(0, 187);
            this.metroButton1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(8, 7);
            this.metroButton1.TabIndex = 6;
            this.metroButton1.Text = "metroButton1";
            // 
            // frmAdministration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1279, 796);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.metroTabControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmAdministration";
            this.Text = "Administration Application";
            this.metroTabControl1.ResumeLayout(false);
            this.tabPageAssign.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.gbAssignEmployee.ResumeLayout(false);
            this.gbAssignEmployee.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeeInformation)).EndInit();
            this.mtpDepartments.ResumeLayout(false);
            this.mtpDepartments.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudRequiredEmployees)).EndInit();
            this.tabpageAutoScheduler.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTemplateView)).EndInit();
            this.tabpageModifyEmployeedetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEditEmployeeDetails)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage tabPageAssign;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.Label lbLoggedInAs;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dgvEmployeeInformation;
        private System.Windows.Forms.GroupBox gbAssignEmployee;
        private MetroFramework.Controls.MetroCheckBox mcbSelectMultiple;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.DateTimePicker dtpShiftDate;
        private MetroFramework.Controls.MetroRadioButton rbMorning;
        private MetroFramework.Controls.MetroRadioButton rbEvening;
        private MetroFramework.Controls.MetroRadioButton rbAfternoon;
        private System.Windows.Forms.Button btnAssignEmployee;
        private System.Windows.Forms.ListBox lbSelectedEmployees;
        private MetroFramework.Controls.MetroComboBox cbbFloor;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FTE;
        private System.Windows.Forms.DataGridViewTextBoxColumn remainingFte;
        private MetroFramework.Controls.MetroTabPage tabpageModifyEmployeedetails;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox tbInput;
        private System.Windows.Forms.Button btnSearchEmployee;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button btnEditEmplyeeData;
        private System.Windows.Forms.DataGridView dgvEditEmployeeDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn employeeid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn BSN;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnOverview;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroTabPage mtpDepartments;
        private System.Windows.Forms.NumericUpDown nudRequiredEmployees;
        private System.Windows.Forms.TextBox tbDepartmentName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbDepartmentName;
        private System.Windows.Forms.Label lbPositionSelect;
        private System.Windows.Forms.ComboBox cbbPosition;
        private System.Windows.Forms.ListBox lbxDepartments;
        private System.Windows.Forms.Button btnDeleteDepartment;
        private System.Windows.Forms.Button btnEditDepartment;
        private System.Windows.Forms.Button btnAddDepartment;
        private MetroFramework.Controls.MetroTabPage tabpageAutoScheduler;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dtpAutoScheduler;
        private System.Windows.Forms.Button btnGenerateAutoSchedule;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private System.Windows.Forms.DataGridView dgvTemplateView;
    }
}