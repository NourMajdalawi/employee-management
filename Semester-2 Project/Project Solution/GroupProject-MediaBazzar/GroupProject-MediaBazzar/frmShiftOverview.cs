﻿using Classes;
using FastMember;
using Org.BouncyCastle.Crypto.Engines;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroupProject_MediaBazzar
{
    public partial class frmShiftOverview : Form
    {
        Store store;
        List<Shift> shifts;
        List<TemplateShift> templateShifts;

        DateTime lower;
        DateTime upper;
        public frmShiftOverview(Store store, List<Shift> shifts)
        {
            //Initialize form and load entered shifts.
            InitializeComponent();
            this.store = store;
            this.shifts = shifts;
            btnPublish.Visible = false;
            dgvAutoSchedulerView.Visible = false;
            btnAddToSchedule.Visible = false;
            btnRemoveFromSchedule.Visible = false;
            cbbDepartment.Visible = false;
            InitializeOverview();
        }
        public frmShiftOverview(Store store, List<TemplateShift> templateShifts, DateTime lowerBound, DateTime upperBound)
        {
            //Initialize form and load entered shifts.
            InitializeComponent();
            upper = upperBound;
            lower = lowerBound;
            this.store = store;
            gbRemove.Visible = false;
            gbShiftPerWeek.Visible = false;
            btnPublish.Visible = true;
            this.templateShifts = templateShifts;
            dgvShiftView.Visible = false;
            showProposedScheduler();
            InitializeCombobox();
        }
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //Intialize the combofox for department ------------
        public void InitializeCombobox()
        {
            List<string> departments = store.DepartmentSelectInitialize();
            cbbDepartment.DataSource = departments;
            foreach (string dpt in departments)
            {
                cbbDepartment.DisplayMember = dpt.ToString();
            }
        }
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //Date picker management ------------
        private DateTime lowerBoundSet(DateTime date)
        {
            while (date.DayOfWeek != DayOfWeek.Monday)
            {
                date = date.AddDays(-1);
            }
            return date;
        }

        private DateTime upperBoundSet(DateTime date)
        {
            while (date.DayOfWeek != DayOfWeek.Sunday)
            {
                date = date.AddDays(1);
            }
            return date;
        }

        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //Initializing the overview -----------
        private void InitializeOverview()
        {
            var groupFloor = new Subro.Controls.DataGridViewGrouper(dgvShiftView);
            //Select date
            DateTime date = dtpShiftView.Value.Date;
            DateTime lower = lowerBoundSet(date);
            DateTime upper = upperBoundSet(date);

            //Fill elements with data
            dgvShiftView.DataSource = store.GetShifts(lower, upper);

            groupFloor.SetGroupOn("floor");
        }

        //Refreshing the overview -------------
        private void cbbDptmntSelect_SelectedValueChanged(object sender, EventArgs e)
        {
            InitializeOverview();
        }

        private void dtpShiftView_ValueChanged(object sender, EventArgs e)
        {
            InitializeOverview();
        }


        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //Removing employees from shift -----------
        private void btnRemove_Click_1(object sender, EventArgs e)
        {
            //get the values id date and daysegment to make sure the correct shift will be deleted
            int selectedrowindex = dgvShiftView.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dgvShiftView.Rows[selectedrowindex];
            int id = Convert.ToInt32(selectedRow.Cells["id"].Value);
            DateTime date = DateTime.Parse((selectedRow.Cells["date"].Value).ToString());
            string daysegment = (selectedRow.Cells["daysegment"].Value).ToString();

            //perform the query
            store.RemoveEmployeeFromShift(id, date, daysegment);

            //Show that the employee has been succesfully removed
            MessageBox.Show("Employee succesfully removed from shift");

            //Update the overview
            InitializeOverview();
        }
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //show proposed schedule -----------
        public void showProposedScheduler()
        {
            var groupDepartment = new Subro.Controls.DataGridViewGrouper(dgvAutoSchedulerView);

            //Fill elements with data
            dgvAutoSchedulerView.DataSource = store.GetTemplateShifts();
            groupDepartment.SetGroupOn("department");
        }
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //publish schedule -----------
        private void btnPublish_Click(object sender, EventArgs e)
        {
            AutoScheduler autoScheduler = new AutoScheduler();
            //autoScheduler.PublishSchedule(templateShifts);
            store.PublishSchedule();
            MessageBox.Show("The template shift has been published successfully!");
            store.DeleteTemporaryTable();
        }
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //Removing an employee from the proposed schedule -----------
        private void btnRemoveFromSchedule_Click(object sender, EventArgs e)
        {
            int selectedrowindex = dgvAutoSchedulerView.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dgvAutoSchedulerView.Rows[selectedrowindex];
            int id = Convert.ToInt32(selectedRow.Cells["employeeid"].Value);
            string daySegment= selectedRow.Cells["daysegment"].Value.ToString();
            store.RemoveEmployeeFromTemporaryShift(id, daySegment);
            UpdateDataGridViewAutoSchedular();
            
        }
        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        //adding an employees to the proposed schedule -----------
        private void btnAddToSchedule_Click(object sender, EventArgs e)
        {
            frmSchedulerAddEmployee addEmployee = new frmSchedulerAddEmployee( cbbDepartment.SelectedItem.ToString(), lower, upper);
            addEmployee.Show();
        }

        private void cbPerDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateDataGridViewAutoSchedular();
        }
        public void UpdateDataGridViewAutoSchedular()
        {
            string department = cbbDepartment.SelectedItem.ToString();
            DataTable dt = store.GetTemplateShiftPerDepartment(department);

            var groupDaySegment = new Subro.Controls.DataGridViewGrouper(dgvAutoSchedulerView);

            //Fill elements with data
            dgvAutoSchedulerView.DataSource = dt;
            groupDaySegment.SetGroupOn("daysegment");
            
        }

        private void frmShiftOverview_FormClosing(object sender, FormClosingEventArgs e)
        {
            store.DeleteTemporaryTable();
        }
    }
}
