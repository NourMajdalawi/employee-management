﻿namespace GroupProject_MediaBazzar
{
    partial class frmEditEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtpBirthday = new System.Windows.Forms.DateTimePicker();
            this.lblBirthday = new MetroFramework.Controls.MetroLabel();
            this.tbusername = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbStreetName = new MetroFramework.Controls.MetroTextBox();
            this.tbZipcode = new MetroFramework.Controls.MetroTextBox();
            this.lblStreetName = new MetroFramework.Controls.MetroLabel();
            this.lblZipcode = new MetroFramework.Controls.MetroLabel();
            this.tbPassword = new MetroFramework.Controls.MetroTextBox();
            this.lblEmployeePass = new MetroFramework.Controls.MetroLabel();
            this.tbEmergencyPhoneNumber = new MetroFramework.Controls.MetroTextBox();
            this.tbPhoneNumber = new MetroFramework.Controls.MetroTextBox();
            this.tbNationality = new MetroFramework.Controls.MetroTextBox();
            this.tbEmail = new MetroFramework.Controls.MetroTextBox();
            this.lblEmail = new MetroFramework.Controls.MetroLabel();
            this.lblEmergency = new MetroFramework.Controls.MetroLabel();
            this.lblPhoneNumber = new MetroFramework.Controls.MetroLabel();
            this.lblAdress = new MetroFramework.Controls.MetroLabel();
            this.lblNationality = new MetroFramework.Controls.MetroLabel();
            this.tbRegion = new MetroFramework.Controls.MetroTextBox();
            this.tbLastName = new MetroFramework.Controls.MetroTextBox();
            this.tbFirstName = new MetroFramework.Controls.MetroTextBox();
            this.lblLastName = new MetroFramework.Controls.MetroLabel();
            this.lblEmployeeName = new MetroFramework.Controls.MetroLabel();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dtpBirthday);
            this.groupBox1.Controls.Add(this.lblBirthday);
            this.groupBox1.Controls.Add(this.tbusername);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.tbStreetName);
            this.groupBox1.Controls.Add(this.tbZipcode);
            this.groupBox1.Controls.Add(this.lblStreetName);
            this.groupBox1.Controls.Add(this.lblZipcode);
            this.groupBox1.Controls.Add(this.tbPassword);
            this.groupBox1.Controls.Add(this.lblEmployeePass);
            this.groupBox1.Controls.Add(this.tbEmergencyPhoneNumber);
            this.groupBox1.Controls.Add(this.tbPhoneNumber);
            this.groupBox1.Controls.Add(this.tbNationality);
            this.groupBox1.Controls.Add(this.tbEmail);
            this.groupBox1.Controls.Add(this.lblEmail);
            this.groupBox1.Controls.Add(this.lblEmergency);
            this.groupBox1.Controls.Add(this.lblPhoneNumber);
            this.groupBox1.Controls.Add(this.lblAdress);
            this.groupBox1.Controls.Add(this.lblNationality);
            this.groupBox1.Controls.Add(this.tbRegion);
            this.groupBox1.Controls.Add(this.tbLastName);
            this.groupBox1.Controls.Add(this.tbFirstName);
            this.groupBox1.Controls.Add(this.lblLastName);
            this.groupBox1.Controls.Add(this.lblEmployeeName);
            this.groupBox1.Location = new System.Drawing.Point(12, 11);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(433, 628);
            this.groupBox1.TabIndex = 94;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Edit";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // dtpBirthday
            // 
            this.dtpBirthday.Location = new System.Drawing.Point(166, 389);
            this.dtpBirthday.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtpBirthday.Name = "dtpBirthday";
            this.dtpBirthday.Size = new System.Drawing.Size(200, 22);
            this.dtpBirthday.TabIndex = 124;
            // 
            // lblBirthday
            // 
            this.lblBirthday.AutoSize = true;
            this.lblBirthday.Location = new System.Drawing.Point(13, 389);
            this.lblBirthday.Name = "lblBirthday";
            this.lblBirthday.Size = new System.Drawing.Size(72, 20);
            this.lblBirthday.TabIndex = 123;
            this.lblBirthday.Text = "Birth date:";
            // 
            // tbusername
            // 
            this.tbusername.Location = new System.Drawing.Point(166, 19);
            this.tbusername.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbusername.Name = "tbusername";
            this.tbusername.Size = new System.Drawing.Size(200, 23);
            this.tbusername.TabIndex = 118;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(11, 20);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(76, 20);
            this.metroLabel1.TabIndex = 117;
            this.metroLabel1.Text = "Username:";
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.DarkCyan;
            this.btnSave.Location = new System.Drawing.Point(75, 559);
            this.btnSave.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(251, 49);
            this.btnSave.TabIndex = 116;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbStreetName
            // 
            this.tbStreetName.Location = new System.Drawing.Point(166, 266);
            this.tbStreetName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbStreetName.Name = "tbStreetName";
            this.tbStreetName.Size = new System.Drawing.Size(200, 23);
            this.tbStreetName.TabIndex = 115;
            // 
            // tbZipcode
            // 
            this.tbZipcode.Location = new System.Drawing.Point(166, 225);
            this.tbZipcode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbZipcode.Name = "tbZipcode";
            this.tbZipcode.Size = new System.Drawing.Size(200, 23);
            this.tbZipcode.TabIndex = 114;
            // 
            // lblStreetName
            // 
            this.lblStreetName.AutoSize = true;
            this.lblStreetName.Location = new System.Drawing.Point(11, 266);
            this.lblStreetName.Name = "lblStreetName";
            this.lblStreetName.Size = new System.Drawing.Size(87, 20);
            this.lblStreetName.TabIndex = 113;
            this.lblStreetName.Text = "Street name:";
            // 
            // lblZipcode
            // 
            this.lblZipcode.AutoSize = true;
            this.lblZipcode.Location = new System.Drawing.Point(11, 225);
            this.lblZipcode.Name = "lblZipcode";
            this.lblZipcode.Size = new System.Drawing.Size(67, 20);
            this.lblZipcode.TabIndex = 112;
            this.lblZipcode.Text = "Zip code:";
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(166, 306);
            this.tbPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.Size = new System.Drawing.Size(200, 23);
            this.tbPassword.TabIndex = 110;
            // 
            // lblEmployeePass
            // 
            this.lblEmployeePass.AutoSize = true;
            this.lblEmployeePass.Location = new System.Drawing.Point(11, 306);
            this.lblEmployeePass.Name = "lblEmployeePass";
            this.lblEmployeePass.Size = new System.Drawing.Size(69, 20);
            this.lblEmployeePass.TabIndex = 108;
            this.lblEmployeePass.Text = "Password:";
            // 
            // tbEmergencyPhoneNumber
            // 
            this.tbEmergencyPhoneNumber.Location = new System.Drawing.Point(166, 436);
            this.tbEmergencyPhoneNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbEmergencyPhoneNumber.Name = "tbEmergencyPhoneNumber";
            this.tbEmergencyPhoneNumber.Size = new System.Drawing.Size(196, 23);
            this.tbEmergencyPhoneNumber.TabIndex = 107;
            // 
            // tbPhoneNumber
            // 
            this.tbPhoneNumber.Location = new System.Drawing.Point(166, 348);
            this.tbPhoneNumber.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbPhoneNumber.Name = "tbPhoneNumber";
            this.tbPhoneNumber.Size = new System.Drawing.Size(200, 23);
            this.tbPhoneNumber.TabIndex = 106;
            // 
            // tbNationality
            // 
            this.tbNationality.Location = new System.Drawing.Point(166, 140);
            this.tbNationality.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbNationality.Name = "tbNationality";
            this.tbNationality.Size = new System.Drawing.Size(200, 23);
            this.tbNationality.TabIndex = 105;
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(166, 488);
            this.tbEmail.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(231, 23);
            this.tbEmail.TabIndex = 104;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(13, 488);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(89, 20);
            this.lblEmail.TabIndex = 103;
            this.lblEmail.Text = "Email adress:";
            // 
            // lblEmergency
            // 
            this.lblEmergency.AutoSize = true;
            this.lblEmergency.Location = new System.Drawing.Point(13, 436);
            this.lblEmergency.Name = "lblEmergency";
            this.lblEmergency.Size = new System.Drawing.Size(127, 20);
            this.lblEmergency.TabIndex = 102;
            this.lblEmergency.Text = "Emergency phone:";
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.AutoSize = true;
            this.lblPhoneNumber.Location = new System.Drawing.Point(13, 348);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(106, 20);
            this.lblPhoneNumber.TabIndex = 101;
            this.lblPhoneNumber.Text = "Phone number:";
            // 
            // lblAdress
            // 
            this.lblAdress.AutoSize = true;
            this.lblAdress.Location = new System.Drawing.Point(11, 185);
            this.lblAdress.Name = "lblAdress";
            this.lblAdress.Size = new System.Drawing.Size(55, 20);
            this.lblAdress.TabIndex = 100;
            this.lblAdress.Text = "Region:";
            // 
            // lblNationality
            // 
            this.lblNationality.AutoSize = true;
            this.lblNationality.Location = new System.Drawing.Point(11, 143);
            this.lblNationality.Name = "lblNationality";
            this.lblNationality.Size = new System.Drawing.Size(77, 20);
            this.lblNationality.TabIndex = 99;
            this.lblNationality.Text = "Nationality:";
            // 
            // tbRegion
            // 
            this.tbRegion.Location = new System.Drawing.Point(166, 185);
            this.tbRegion.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbRegion.Name = "tbRegion";
            this.tbRegion.Size = new System.Drawing.Size(200, 23);
            this.tbRegion.TabIndex = 98;
            // 
            // tbLastName
            // 
            this.tbLastName.Location = new System.Drawing.Point(166, 102);
            this.tbLastName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(200, 23);
            this.tbLastName.TabIndex = 97;
            // 
            // tbFirstName
            // 
            this.tbFirstName.Location = new System.Drawing.Point(166, 62);
            this.tbFirstName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(200, 23);
            this.tbFirstName.TabIndex = 96;
            // 
            // lblLastName
            // 
            this.lblLastName.AutoSize = true;
            this.lblLastName.Location = new System.Drawing.Point(11, 102);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(75, 20);
            this.lblLastName.TabIndex = 95;
            this.lblLastName.Text = "Last name:";
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.AutoSize = true;
            this.lblEmployeeName.Location = new System.Drawing.Point(11, 62);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(76, 20);
            this.lblEmployeeName.TabIndex = 94;
            this.lblEmployeeName.Text = "First name:";
            // 
            // frmEditEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(453, 647);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmEditEmployee";
            this.Text = "Edit Employee";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroTextBox tbStreetName;
        private MetroFramework.Controls.MetroTextBox tbZipcode;
        private MetroFramework.Controls.MetroLabel lblStreetName;
        private MetroFramework.Controls.MetroLabel lblZipcode;
        private MetroFramework.Controls.MetroTextBox tbPassword;
        private MetroFramework.Controls.MetroLabel lblEmployeePass;
        private MetroFramework.Controls.MetroTextBox tbEmergencyPhoneNumber;
        private MetroFramework.Controls.MetroTextBox tbPhoneNumber;
        private MetroFramework.Controls.MetroTextBox tbNationality;
        private MetroFramework.Controls.MetroLabel lblEmail;
        private MetroFramework.Controls.MetroLabel lblEmergency;
        private MetroFramework.Controls.MetroLabel lblPhoneNumber;
        private MetroFramework.Controls.MetroLabel lblAdress;
        private MetroFramework.Controls.MetroLabel lblNationality;
        private MetroFramework.Controls.MetroTextBox tbRegion;
        private MetroFramework.Controls.MetroTextBox tbLastName;
        private MetroFramework.Controls.MetroTextBox tbFirstName;
        private MetroFramework.Controls.MetroLabel lblLastName;
        private MetroFramework.Controls.MetroLabel lblEmployeeName;
        private System.Windows.Forms.Button btnSave;
        private MetroFramework.Controls.MetroTextBox tbusername;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.DateTimePicker dtpBirthday;
        private MetroFramework.Controls.MetroLabel lblBirthday;
        private MetroFramework.Controls.MetroTextBox tbEmail;
    }
}