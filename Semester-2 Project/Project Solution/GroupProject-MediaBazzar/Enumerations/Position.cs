﻿using System;

public enum Position
{
    RetailWorker,
    DepotWorker,
    Manager,
    Admin,
    Security,
    None
}
