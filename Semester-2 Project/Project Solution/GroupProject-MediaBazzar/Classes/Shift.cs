﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Classes;

namespace GroupProject_MediaBazzar
{
    public class Shift
    {

        public int id { get; }
        public int employeeId { get; }
        public string employeeName { get;  }
        public DateTime date { get; }
        public string daySegment { get; }
        public string floor { get; }
        public Shift()
        {

        }

        public Shift(int id, int employeeId, DateTime date, string daySegment, string floor)
        {
        
            //Check if id is bigger than -1 to see if we want to assign it
            if(id > -1)
            {
                this.id = id;
            }
            this.employeeId = employeeId;
            
            this.date = date;
            this.daySegment = daySegment;
            this.floor = floor;
        }
    }
}
