﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProject_MediaBazzar
{
    public class Floor
    {
        private string name;
        private int number;
        List<Employee> employees;
        List<Product> products;

        public Floor (String name, int number)
        {
            this.name = name;
            this.number = number;
            employees = new List<Employee>();
            products = new List<Product>();
        }
    }
}
