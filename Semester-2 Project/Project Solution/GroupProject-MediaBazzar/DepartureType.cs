﻿using System;

public enum DepartureType
{
    Fired,
    Quit
}
