<script>
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');

        var calendar = new FullCalendar.Calendar(calendarEl, {


            plugins: [ 'interaction', 'dayGrid', 'list', 'googleCalendar' ],

            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,listYear'
            },
            businessHours: {
                // days of week. an array of zero-based day of week integers (0=Sunday)
                daysOfWeek: [ 1, 2, 3, 4, 5 ], // Monday - Friday

            },

            displayEventTime: false, // don't show the time column in list view


            //Add events to the calendar
          events:[
	    <?php
        include "classes/Shift.php";
        $shifts = new Shift();
	    foreach($shifts->GetShifts() as $shift)
	    {
		    echo $shift;
	    }
	    ?>
    ]
    });

    calendar.render();
    });

</script>
