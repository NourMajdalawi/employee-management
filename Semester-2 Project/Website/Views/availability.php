<!DOCTYPE html>
<html lang="en">

<head>
    <?php include "classes/Availability.php";
    $availability = new Availability();
    $availability->InsetAvailability();
    ?>

</head>
  <body>
        <table class="tb_header"  border="50" cellpadding="10" cellspacing="0" width="600" align="center">
            <td  colspan="5" class="tableHead">Shift Times</td>

            <tr class="columns">
                <th>Morning</th>
                <th>Afternoon</th>
                <th>Evening</th>
            </tr>
            <tr class="columns">
                <th>8.00 - 12.30</th>
                <th>12.30 - 17.00</th>
                <th>17.00 - 21.30</th>
            </tr>
        </table>
        <br><br>
        <div class="container-fluid">
        <form action="" name="availabilityForm" id="availabilityForm" method="post">

            <div class="grid-container">
                <div>Monday</div>
                <div>Tuesday</div>
                <div>Wednesday</div>
                <div>Thursday</div>
                <div>Friday</div>
                <div>Saturday</div>
                <div>Sunday</div>
                <label><input type="checkbox" name="mondayMorning"      id="mondayMorning"      value="mondayMorning"      <?php if($_POST['monMorning'] == 1)   echo "checked='checked'"; ?> /> Morning</label>
                <label><input type="checkbox" name="tuesdayMorning"     id="tuesdayMorning"     value="tuesdayMorning"     <?php if($_POST['tueMorning'] == 1)   echo "checked='checked'"; ?>/>  Morning</label>
                <label><input type="checkbox" name="wednesdayMorning"   id="wednesdayMorning"   value="wednesdayMorning"   <?php if($_POST['wedMorning'] == 1)   echo "checked='checked'"; ?>/>  Morning</label>
                <label><input type="checkbox" name="thursdayMorning"    id="thursdayMorning"    value="thursdayMorning"    <?php if($_POST['thuMorning'] == 1)   echo "checked='checked'"; ?>/>   Morning</label>
                <label><input type="checkbox" name="fridayMorning"      id="fridayMorning"      value="fridayMorning"      <?php if($_POST['friMorning'] == 1)   echo "checked='checked'"; ?>/>  Morning</label>
                <label><input type="checkbox" name="saturdayMorning"    id="saturdayMorning"    value="saturdayMorning"    <?php if($_POST['satMorning'] == 1)   echo "checked='checked'"; ?>/>  Morning</label>
                <label><input type="checkbox" name="sundayMorning"      id="sundayMorning"      value="sundayMorning"      <?php if($_POST['sunMorning'] == 1)   echo "checked='checked'"; ?>/>  Morning</label>
                <label><input type="checkbox" name="mondayAfternoon"    id="mondayAfternoon"    value="mondayAfternoon"    <?php if($_POST['monAfternoon'] == 1) echo "checked='checked'"; ?>/>   Afternoon</label>
                <label><input type="checkbox" name="tuesdayAfternoon"   id="tuesdayAfternoon"   value="tuesdayAfternoon"   <?php if($_POST['tueAfternoon'] == 1) echo "checked='checked'"; ?>/>  Afternoon</label>
                <label><input type="checkbox" name="wednesdayAfternoon" id="wednesdayAfternoon" value="wednesdayAfternoon" <?php if($_POST['wedAfternoon'] == 1) echo "checked='checked'"; ?>/>   Afternoon</label>
                <label><input type="checkbox" name="thursdayAfternoon"  id="thursdayAfternoon"  value="thursdayAfternoon"  <?php if($_POST['thuAfternoon'] == 1) echo "checked='checked'"; ?>/>  Afternoon</label>
                <label><input type="checkbox" name="fridayAfternoon"    id="fridayAfternoon"    value="fridayAfternoon"    <?php if($_POST['friAfternoon'] == 1) echo "checked='checked'"; ?>/>  Afternoon</label>
                <label><input type="checkbox" name="saturdayAfternoon"  id="saturdayAfternoon"  value="saturdayAfternoon"  <?php if($_POST['satAfternoon'] == 1) echo "checked='checked'"; ?>/>  Afternoon</label>
                <label><input type="checkbox" name="sundayAfternoon"    id="sundayAfternoon"    value="sundayAfternoon"    <?php if($_POST['sunAfternoon'] == 1) echo "checked='checked'"; ?>/> Afternoon</label>
                <label><input type="checkbox" name="mondayEvening"      id="mondayEvening"      value="mondayEvening"      <?php if($_POST['monEvening'] == 1)   echo "checked='checked'"; ?>/>  Evening</label>
                <label><input type="checkbox" name="tuesdayEvening"     id="tuesdayEvening"     value="tuesdayEvening"     <?php if($_POST['tueEvening'] == 1)   echo "checked='checked'"; ?>/>  Evening</label>
                <label><input type="checkbox" name="wednesdayEvening"   id="wednesdayEvening"   value="wednesdayEvening"   <?php if($_POST['wedEvening'] == 1)   echo "checked='checked'"; ?>/>   Evening</label>
                <label><input type="checkbox" name="thursdayEvening"    id="thursdayEvening"    value="thursdayEvening"    <?php if($_POST['thuEvening'] == 1)   echo "checked='checked'"; ?>/>   Evening</label>
                <label><input type="checkbox" name="fridayEvening"      id="fridayEvening"      value="fridayEvening"      <?php if($_POST['friEvening'] == 1)   echo "checked='checked'"; ?>/>  Evening</label>
                <label><input type="checkbox" name="saturdayEvening"    id="saturdayEvening"    value="saturdayEvening"    <?php if($_POST['satEvening'] == 1)   echo "checked='checked'"; ?>/>  Evening</label>
                <label><input type="checkbox" name="sundayEvening"      id="sundayEvening"      value="sundayEvening"      <?php if($_POST['sunEvening'] == 1)   echo "checked='checked'"; ?>/>  Evening</label>
            </div>
            <input class="button_availability" type="submit" name="submit" value="Submit">
        </form>
       </div>

  </body>
<script defer src="Scripts/availabilityscript.js"></script>
</html>