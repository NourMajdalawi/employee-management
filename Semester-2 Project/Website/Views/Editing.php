<!DOCTYPE html>
<html lang="en">
<br><br>
<br><br>
<head>

    <?php
    include "classes/EditPersonalDetails.php";
    $edit = new EditPersonalDetails();
    $edit->UpdatePersonalDetails();
    include "classes/Loaddata.php";
    $data = new Loaddata();
    $data->LoadEmployeeDate();
    ?>


</head>

<body>
        <form action='' name="editForm" id="editForm" method="POST" onsubmit="">

            <!-- just to see if the page will show hello and the name (which it does) -->
            <table class="tb_header"  border="50" cellpadding="10" cellspacing="0" width="600" align="center">

                <td  colspan="5" class="tableHead">Modify Personal Details</td>

                <tr class="columns">
                    <td><label>EmailAddress</label></td>
                    <td><input type="text" name="email" id="emailAddress" class="txtField" value="<?php echo ($_POST['emailAddress']) ?>" onkeyup="mailvalidate()"></td>
                </tr>

                <tr class="columns">
                    <td><label>Spokenlanguages</label></td>
                    <td><input type="text" name="spokenlanguages" id="spokenlanguges" class="txtField" value="<?php echo ($_POST['spokenlanguages']) ?>"></td>
                </tr>
                <tr class="columns">
                    <td><label>Region</label></td>
                    <td><input type="text" name="region" id="region" class="txtField" value="<?php echo ($_POST['region']) ?>"></td>
                </tr>
                <tr class="columns">
                    <td><label>Zipcode</label></td>
                    <td><input type="text" name="zipcode" id="zipcode" class="txtField" value="<?php echo ($_POST['zipcode']) ?>"></td>
                </tr>
                <tr class="columns">
                    <td><label>Streetname</label></td>
                    <td><input type="text" name="streetname" id="streetname" class="txtField" value="<?php echo ($_POST['streetname']) ?>"></td>
                </tr>
                <tr class="columns">
                    <td><label>Phonenumber</label></td>
                    <td><input type="text" name="phonenumber" id="phonenumber" class="txtField" value="<?php echo ($_POST['phonenumber']) ?>"></td>
                </tr>
                <tr class="columns">
                    <td><label>Emergencyphonenumber</label></td>
                    <td><input type="text" name="emergencyphonenumber" id="emergencyphonenumber" class="txtField" value="<?php echo ($_POST['emergencyphonenumber']) ?>"></td>
                </tr>
                <tr class="columns">
                    <td><label>Spouse</label></td>
                    <td><input type="checkbox" name="spouse" id="spouse" value="yes" <?php echo ($_POST['spouse']==1 ? 'checked' : '');?>></td>
                </tr>
                <tr align="center">
                    <div class="hidden" id ="error"></div>
                      <?php if(isset($err)){echo $err;} ?><br>
                    <td colspan="2"><input class="button" type="submit" name="submit" value="Submit"></td>
                </tr>
            </table>

        </form>
</body>
<script defer src="Scripts/editemployeescript.js"></script>
</html>