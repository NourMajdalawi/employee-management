<?php

class  Login extends DataHelper{
    public  function Run(){

        $pdo = $this->createConnection();
        //To redirect to another page when the login/signup has been successful
        function RedirectToURL($url, $waitmsg = 0.4)
        {
            header("Refresh:$waitmsg; URL= $url");
            exit;
        }

//checks if the loginusername and password is filled in
        if (isset($_POST['username']) and isset($_POST['password'])){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $sql = 'SELECT * FROM employee WHERE username=:username AND  password=:password';
            $sth = $pdo->prepare($sql);
            $sth->execute([':username' => $username, ':password' => $password]);
            $result = $sth->fetchAll();
            foreach ( $result as $employee )
            {
                if ( $employee[2] == $username && $employee[3] == $password ) {
//Sets the session username into the name of the current user
                    $_SESSION['username'] = $employee[2];
                    $_SESSION['firstname'] = $employee[5];
                    $_SESSION['lastname'] = $employee[6];
                    $_SESSION['userid'] = $employee[0];
                    $_session['loggedin'] = true;

                    $sql = 'SELECT * FROM contract WHERE employeeid=:employeeid';
                    $sth = $pdo->prepare($sql);
                    $sth->execute([':employeeid' => $employee[0]]);
                    $result = $sth->fetchAll();
                    foreach ($result as $fte){
                        $_SESSION['FTE'] = $fte[6];
                    }
                    RedirectToURL( "index.php?page=shift", 0);
                }
            }

            RedirectToURL( "index.php?page=login", 0 );




        }
    }
}