<?php
class EditPersonalDetails extends DataHelper {

    public  function  UpdatePersonalDetails(){
        try{
            $pdo = $this->createConnection();
            $err = "" ;
            $GLOBALS['err'] = "";

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                // The request is using the POST method

                if (!empty($_POST['email'])){
                    $email = $_POST['email'];
                }
                if (!empty($_POST['spokenlanguages'])) {
                    $spokenlanguages = $_POST['spokenlanguages'];
                }
                if (!empty($_POST['region'])) {
                    $region = $_POST['region'];
                }
                if (!empty($_POST['zipcode'])) {
                    $zipcode = $_POST['zipcode'];
                }
                if (!empty($_POST['streetname'])) {
                    $streetname = $_POST['streetname'];
                }
                if (!empty($_POST['phonenumber'])) {
                    $phonenumber= $_POST['phonenumber'];
                }
                if (!empty($_POST['emergencyphonenumber'])) {
                    $emergencyphonenumber = $_POST['emergencyphonenumber'];
                }
                if(empty($_POST['spouse'])) {
                    $_POST['spouse'] = 0;
                }
                else{
                    $_POST['spouse'] = 1;
                }

                $spokenlanguages = $_POST['spokenlanguages'];
                $email = $_POST['email'];
                $streetname = $_POST['streetname'];
                $region = $_POST['region'];
                $phonenumber =$_POST['phonenumber'];
                $emergencyphonenumber = $_POST['emergencyphonenumber'];
                $spouse = $_POST['spouse'];



                $employeeid = $_SESSION['userid'];
                $sql = 'UPDATE employee SET emergencyphonenumber=:emergencyphonenumber, phonenumber=:phonenumber, streetname=:streetname,
 zipcode=:zipcode, region=:region,spokenlanguages=:spokenlanguages, emailaddress= :email, spouse= :spouse WHERE id=:employeeid' ;
                $sth = $pdo->prepare($sql);
                $sth->execute([':employeeid'=> $employeeid, ':emergencyphonenumber'=> $emergencyphonenumber,
                    ':phonenumber'=>$phonenumber, ':streetname'=>$streetname, ':zipcode'=>$zipcode, ':region' => $region,
                    ':spokenlanguages' => $spokenlanguages, ':email'=>$email, ':spouse'=>$spouse]);
                $result = $sth->fetchAll();

                if($result!== false){

                    $_SESSION['emailAddress'] = $email;
                    $_SESSION['spokenlanguages'] = $spokenlanguages;
                    $_SESSION['region'] = $region;
                    $_SESSION['zipcode'] = $zipcode;
                    $_SESSION['streetname'] = $streetname;
                    $_SESSION['phonenumber'] = $phonenumber;
                    $_SESSION['emergencyphonenumber'] = $emergencyphonenumber;
                    $_SESSION['spouse'] = $spouse;
                    $_SESSION['result']= $result;
                }
               $pdo = null;
//executes the query
            }

        }
        catch (PDOException $ex){
            echo  $ex->getMessage();
        }
    }
}
