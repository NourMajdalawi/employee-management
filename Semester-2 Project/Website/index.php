
<?php
echo "<html>";

require 'includes/init.php';

/*To check whether it has a value or not? If it is set or not?*/
if( isset($_GET['page']) )
{
    $page = $_GET['page'];
}
else
{
    $page = "login";
}
//include the head so its the same in every page
include "Views/head.php";

echo "<body>";
//see which page needs to be shown
switch($page) {
case "login":
include "Views/login.php";
break;

case "vacationdays":
include "Views/header.php";
include "Views/vacationdays.php";
break;

case "shift":
include "Views/header.php";
include "Views/shift.php";
break;

case "logout";
include "Views/header.php";
include "Views/logout.php";
break;

case "EditEmployeeDetails";
include "Views/header.php";
include "Views/Editing.php";
break;

case "availability";
include "Views/header.php";
include "Views/availability.php";
break;

case "password";
include "Views/header.php";
include "Views/password.php";
break;
}

echo "</body>";
echo "</html>";

