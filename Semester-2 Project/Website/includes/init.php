<?php

spl_autoload_register('prjAutoloader');
// the classs names should be same as file names
function prjAutoloader ($className) {
    $path = 'classes/';
    $extension = '.php';
    $fileName = $path . $className . $extension;

    if (!file_exists($fileName)) {
        return false;
    }

    include_once $path . $className . $extension;
}
?>