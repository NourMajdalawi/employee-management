const email = document.getElementById('emailAddress');
const spokenlanguages  = document.getElementById('spokenlanguges');
const region = document.getElementById('region');
const zipcode = document.getElementById('zipcode');
const streetname = document.getElementById('streetname');
const phonenumber = document.getElementById('phonenumber');
const emergencyphonenumber = document.getElementById('emergencyphonenumber');
const form = document.getElementById('editForm');
const changepasswordFrom = document.getElementById('changePasswordForm');
const errorElement = document.getElementById('error');

form.addEventListener('submit', (e) => {

    let messages = [];
    if (email.value === '' || email.value == null){
        messages.push('Email is empty')
    }

    if(spokenlanguages.value === '' || spokenlanguages.value == null){
        messages.push('SpokenLanguages is empty')
    }
    if(region.value === '' || region.value == null){
        messages.push('Region is empty')
    }
    if(zipcode.value === '' || zipcode.value == null){
        messages.push('Zipcode is empty')
    }
    if(streetname.value === '' || streetname.value == null){
        messages.push('Streetname is empty')
    }
    if(phonenumber.value === '' || phonenumber.value == null){
        messages.push('Phonenumber is empty')
    }
    if(emergencyphonenumber.value === '' || emergencyphonenumber.value == null){
        messages.push('Emergencyphonenumber is empty')
    }

    if(messages.length > 0){
        errorElement.innerText = messages.join(', ');
        e.preventDefault();
    }
    else{
        $.ajax({
            url: 'classes/EditPersonalDetails.php',
            method:'POST',
            success: function() {
                alert("Editing done successfully!");
            }
        });

    }
});
function validatepassword(){
    var psw = document.getElementById('psw').value;
    var psw_repeat =document.getElementById('pswcon').value;
    if(psw != psw_repeat){
        document.changePasswordForm.password.style.border="3px dashed red";
        document.changePasswordForm.repeatpassword.style.border="3px dashed red";
        document.getElementById('submit').disabled = true;

    } else {
        document.changePasswordForm.password.style.border="3px dashed green";
        document.changePasswordForm.repeatpassword.style.border="3px dashed green";
        document.getElementById('submit').disabled = false;
    }
}

function testmail() {
    var str= document.getElementById('email').value;
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+[^<>()\.,;:\s@\"]{2,})$/;
    return re.test(String(str).toLowerCase());
}
function mailvalidate() {
    if(testmail()&& document.getElementById('email').value.length>10){
        document.getElementById('email').style.border="3px dashed green";}

    else{
        document.getElementById('email').style.border="3px dashed red";}
}


